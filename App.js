import React, { Component } from 'react';
import {
  createSwitchNavigator,
  createStackNavigator,
  createAppContainer,
} from 'react-navigation';
import { Root } from 'native-base';
import Photo from './src/duta/Photo';
import Welcome from './src/welcome/Welcome';
import Login from './src/auth/Login';
import Register from './src/auth/Register';
import Forget from './src/forget/Forget';
import First from './src/first/Index';
import Quote from './src/quote/Quote';
import Bicara from './src/bicara/Bicara';
import Tenang from './src/tenang/Tenang';
import Semangat from './src/semangat/Semangat';
import Sehat from './src/sehat/Sehat';
import Stamina from './src/stamina/Stamina';
import Metime from './src/metime/Metime';
import Music from './src/music/Music';
import Menu from './src/menu/Menu';
import Auth from './src/auth/Auth';
import Slide from './src/slide/Slide';
import PdfView from './src/pdf/Pdf';
import Player from './src/youtube/Player';
import ListResep from './src/sehat/ListResep';
import Chat from './src/chat/Chat';
import DisplayUser from './src/chat/DisplayUser';
import Lsm from './src/bicara/Lsm';
import Duta from './src/duta/Duta';
import Cannabis from './src/duta/Cannabis';
import Opiat from './src/duta/Opiat';
import Ats from './src/duta/Ats';
import Halusinogen from './src/duta/Halusinogen';
import Adiktif from './src/duta/Adiktif';
import Inspiratif from './src/semangat/Inspiratif';
import Treatment from './src/sehat/Treatment';

const authNav = createStackNavigator({
  Login, Register, Forget, Auth,
}, {
  initialRouteName: 'Auth',
  headerMode: 'none',
});

const mainNav = createStackNavigator({
  Quote,
  Chat,
  Bicara,
  Tenang,
  Semangat,
  Sehat,
  Stamina,
  Lsm,
  Metime,
  Music,
  Menu,
  Motivasi: Slide,
  PdfView,
  Player,
  ListResep,
  Resep: Slide,
  DisplayUser,
  Duta,
  Cannabis,
  Opiat,
  Ats,
  Photo,
  Halusinogen,
  Adiktif,
  Inspiratif,
  Treatment,
}, {
  initialRouteName: 'Menu',
  headerMode: 'none',
});

const transNav = createSwitchNavigator({
  First,
  mainNav,
}, {
  initialRouteName: 'First',
});

const main = createSwitchNavigator({
  authNav,
  transNav,
  Welcome,
}, {
  initialRouteName: 'Welcome',
});

const MainApp = createAppContainer(main);
export default class App extends Component {
  render() {
    return (
      <Root>
        <MainApp />
      </Root>
    );
  }
}
