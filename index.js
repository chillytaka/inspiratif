/**
 * @format
 */

import { AppRegistry } from 'react-native';
import TrackPlayer from 'react-native-track-player';
import App from './App';
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => App);
// eslint-disable-next-line global-require
// TrackPlayer.registerPlaybackService(() => require('./service'));
TrackPlayer.registerPlaybackService(() => (
  async () => {
    TrackPlayer.addEventListener('remote-pause', () => {
      TrackPlayer.pause();
    });
    TrackPlayer.addEventListener('remote-play', () => {
      TrackPlayer.play();
    });
    TrackPlayer.addEventListener('remote-stop', () => {
      TrackPlayer.destroy();
    });

    let ayah = Math.floor(Math.random() * 6236) + 1;
    TrackPlayer.addEventListener('playback-track-changed', async ({ nextTrack }) => {
      const test = await TrackPlayer.getCurrentTrack();
      if (test !== '2') {
        TrackPlayer.add({
          id: ayah,
          url: `https://cdn.alquran.cloud/media/audio/ayah/ar.alafasy/${ayah}`,
          title: 'Murottal',
          artist: 'Alquran Cloud',
          artwork: 'https://images.unsplash.com/photo-1535552081922-631f7dea1e9d',
        }).then(async () => {
          ayah += 1;
          if (nextTrack === '1') {
            TrackPlayer.skipToNext();
            TrackPlayer.play();
          }
        });
      }
    });

    TrackPlayer.addEventListener('playback-error', () => {
      TrackPlayer.skipToNext();
    });
  }
));
