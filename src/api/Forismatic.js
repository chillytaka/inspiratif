import Error from '../reusable/ErrorToast';

const server = 'http://api.forismatic.com/api/1.0/';
export default function getQuote() {
  return new Promise(async (res) => {
  // assign required body to formdata
  // eslint-disable-next-line no-undef
    const item = new FormData();

    await item.append('format', 'json'); // format of the api
    await item.append('method', 'getQuote'); // function
    await item.append('lang', 'en'); // language

    fetch(`${server}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: item,
    })
      .then(resp => resp.json())
      .then(respJson => res(respJson))
      .catch(error => Error(error));
  });
}
