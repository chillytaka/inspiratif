import Error from '../reusable/ErrorToast';

const server = 'https://api.unsplash.com//photos/random?orientation=portrait&client_id=ad30e8e142621ab27d44ea4d7b30b48bf4cdd9b8c3a379226c9a39800866d88b&query=dark nature';
export default function getPhoto() {
  return new Promise(async (res) => {
  // assign required body to formdata
    fetch(`${server}`)
      .then(resp => resp.json())
      .then(respJson => res(respJson.urls.regular))
      .catch(error => Error(error));
  });
}
