import React, { Component } from 'react';
import {
  StatusBar, StyleSheet, View, ScrollView, Dimensions,
} from 'react-native';
import { Container, Content } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../val/colors';
import Login from './Login';
import Register from './Register';
import TextButton from '../reusable/TextButton';

const styles = StyleSheet.create({
  separator: {
    height: '10%',
  },
});

const { width, height } = Dimensions.get('window');
export default class Auth extends Component {
  constructor(props) {
    super(props);

    this.state = {
      login: true,
      register: false,
    };
  }

  toLogin = () => {
    this._welcome.scrollTo({ x: 0, y: 0, animated: true });
    this.setState({ login: true, register: false });
  }

  toRegister = () => {
    this._welcome.scrollTo({ x: width, y: 0, animated: true });
    this.setState({ login: false, register: true });
  }

  render() {
    const { login, register } = this.state;
    return (
      <Container>
        <StatusBar hidden />
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{ height, width }}
        >
          <Content>
            <View style={styles.separator} />
            <View style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
            }}
            >
              <TextButton active={login} text="Login" onPress={this.toLogin} />
              <TextButton active={register} text="Register" onPress={this.toRegister} />
            </View>
            <ScrollView
              horizontal
              pagingEnabled
              showsHorizontalScrollIndicator={false}
              scrollEnabled={false}
              ref={(c) => { this._welcome = c; }}
            >
              <Login />
              <Register />
            </ScrollView>
          </Content>
        </LinearGradient>
      </Container>
    );
  }
}
