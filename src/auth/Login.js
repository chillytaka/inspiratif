import React, { Component } from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import { Text, Button } from 'native-base';
import { withNavigation } from 'react-navigation';
import { material } from 'react-native-typography';
import firebase from 'react-native-firebase';
import Loading from '../reusable/Loading';
import colors from '../val/colors';
import LoginField from './LoginField';
import Error from '../reusable/ErrorToast';

const styles = StyleSheet.create({
  loginText: {
    alignSelf: 'center',
    marginTop: '10%',
  },
  quoteText: {
    margin: 10,
    marginTop: '10%',
    textAlign: 'center',
  },
  loginSeparator: {
    height: '8%',
  },
  loginButton: {
    backgroundColor: colors.accent,
    alignSelf: 'center',
    marginTop: 20,
    elevation: 5,
  },
  separator: {
    height: '5%',
  },
});

const { width, height } = Dimensions.get('window');

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      isLoading: false,
    };
  }

  loginTextChanged = (email) => {
    this.setState({ email });
  }

  passTextChanged = (password) => {
    this.setState({ password });
  }

  onLogin = () => {
    const { email, password } = this.state;
    const { navigation } = this.props;
    this.setState({ isLoading: true });
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        navigation.navigate('transNav');
      })
      .catch((error) => {
        Error(error.message);
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { isLoading } = this.state;
    return (
      <View style={{ height, width }}>
        <Text style={[styles.loginText, material.display2]}>Login</Text>
        <Text style={[styles.quoteText, material.titleWhite]}>
            Kebahagiaan dan makna hidup
          {'\n'}
            ada di tangan kita sendiri
        </Text>
        <View style={styles.loginSeparator} />

        <LoginField
          loginTextChange={this.loginTextChanged}
          passTextChange={this.passTextChanged}
        />

        <Button style={styles.loginButton} rounded onPress={this.onLogin}>
          <Text>Login</Text>
        </Button>
        <Loading isLoading={isLoading} />
      </View>
    );
  }
}

export default withNavigation(Login);
