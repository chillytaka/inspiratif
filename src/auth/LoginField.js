import React, { Component } from 'react';
import { Card, Item, Input } from 'native-base';
import PropTypes from 'prop-types';


export default class LoginField extends Component {
  static propTypes = {
    loginTextChange: PropTypes.func.isRequired,
    passTextChange: PropTypes.func.isRequired,
  }

  render() {
    const { loginTextChange, passTextChange } = this.props;
    return (
      <Card style={{ width: '90%', alignSelf: 'center' }}>
        <Item>
          <Input
            placeholder="Email"
            onChangeText={(text) => { loginTextChange(text); }}
          />
        </Item>
        <Item>
          <Input
            placeholder="Password"
            secureTextEntry
            onChangeText={(text) => { passTextChange(text); }}
          />
        </Item>
      </Card>
    );
  }
}
