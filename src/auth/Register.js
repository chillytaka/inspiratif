import React, { Component } from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import { Text, Button } from 'native-base';
import { material } from 'react-native-typography';
import { withNavigation } from 'react-navigation';
import firebase from 'react-native-firebase';
import colors from '../val/colors';
import RegisterField from './RegisterField';
import Error from '../reusable/ErrorToast';
import Loading from '../reusable/Loading';

const styles = StyleSheet.create({
  signupText: {
    alignSelf: 'center',
    marginTop: '5%',
  },
  separator: {
    height: '5%',
  },
  signupButton: {
    backgroundColor: colors.accent,
    alignSelf: 'center',
    marginTop: 20,
    elevation: 5,
  },
});

const { width, height } = Dimensions.get('window');
class Register extends Component {
  constructor(props) {
    super(props);
    this.ref = firebase.firestore().collection('users');
    this.state = {
      username: '',
      tglLahir: '',
      domisili: '',
      telp: '',
      email: '',
      pass: '',
      confrpass: '',
      isLoading: false,
    };
  }

  // save inputted text to state
changedText = (label, text) => {
  this.setState({ [label]: text });
}

// registering user
onRegister = () => {
  const {
    pass, email, confrpass, domisili, tglLahir, telp, username,
  } = this.state;
  const { navigation } = this.props;
  this.setState({ isLoading: true });
  if (pass !== confrpass) {
    Error('password and confirm password do not match');
  } else {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, pass) // register user on firebase auth
      .then((data) => {
        const userId = data.user.uid;

        // add displayName
        data.user.updateProfile({
          displayName: username,
        });

        // add user related info on firestore
        this.ref.add({
          domisili,
          lahir: tglLahir,
          phone: telp,
          userId,
          username,
        })
          .then(() => navigation.navigate('transNav'))
          .catch((error) => {
            Error(error.message);
            this.setState({ isLoading: false });
          });
      })
      .catch((error) => {
        Error(error.message);
        this.setState({ isLoading: false });
      });
  }
}

render() {
  const { isLoading } = this.state;
  return (
    <View
      style={{ width, height: height + 100 }}
    >
      <Text style={[styles.signupText, material.display2]}>Sign Up</Text>
      <View style={styles.separator} />
      <RegisterField changedText={this.changedText} />
      <Button
        rounded
        style={styles.signupButton}
        onPress={this.onRegister}
      >
        <Text>Register</Text>
      </Button>
      <Loading isLoading={isLoading} />
    </View>
  );
}
}

export default withNavigation(Register);
