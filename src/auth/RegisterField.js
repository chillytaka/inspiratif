import React, { Component } from 'react';
import {
  Card, Item, DatePicker, Input, Label,
} from 'native-base';
import PropTypes from 'prop-types';

export default class RegisterField extends Component {
  static propTypes ={
    changedText: PropTypes.func.isRequired,
  }

  render() {
    const { changedText } = this.props;
    return (
      <Card style={{ width: '90%', alignSelf: 'center' }}>
        <Item>
          <Input
            placeholder="Username"
            onChangeText={text => changedText('username', text)}
          />
        </Item>
        <Item>
          <Label>Tanggal Lahir : </Label>
          <DatePicker onDateChange={date => changedText('tglLahir', date)} />
        </Item>
        <Item>
          <Input
            placeholder="Domisili"
            onChangeText={text => changedText('domisili', text)}
          />
        </Item>
        <Item>
          <Input
            placeholder="No. Telp"
            keyboardType="numeric"
            onChangeText={text => changedText('telp', text)}
          />
        </Item>
        <Item>
          <Input
            placeholder="Email"
            keyboardType="email-address"
            onChangeText={text => changedText('email', text)}
          />
        </Item>
        <Item>
          <Input
            placeholder="Password"
            secureTextEntry
            onChangeText={text => changedText('pass', text)}
          />
        </Item>
        <Item>
          <Input
            placeholder="Confirm Password"
            secureTextEntry
            onChangeText={text => changedText('confrpass', text)}
          />
        </Item>
      </Card>
    );
  }
}
