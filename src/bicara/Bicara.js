import React, { Component } from "react";
import { Container } from "native-base";
import LinearGradient from "react-native-linear-gradient";
import Title from "../reusable/Title";
import colors from "../val/colors";
import CustomButton from "../reusable/CustomButton";
import BackButton from "../reusable/BackButton";
import Logo from "../reusable/Logo";

export default class Bicara extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{ height: "100%", padding: 16 }}
        >
          <BackButton />

          <Logo />

          <Title title="Aku Butuh Bicara !" />

          <CustomButton
            text="Psikolog"
            icon="person"
            onPress={() => navigation.navigate("DisplayUser")}
          />
          <CustomButton
            text="Lembaga Swadaya Masyarakat"
            icon="pin"
            onPress={() => navigation.navigate("Lsm")}
          />
          <CustomButton
            text="Duta Anti Narkoba"
            icon="body"
            onPress={() => navigation.navigate("Duta")}
          />
        </LinearGradient>
      </Container>
    );
  }
}
