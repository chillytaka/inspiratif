import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import { View, StyleSheet, Dimensions } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../val/colors';
import BackButton from '../reusable/BackButton';
import Loading from '../reusable/Loading';
import LsmItem from './LsmItem';

const { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  backStyle: {
    left: 16,
    top: 16,
    elevation: 1,
    zIndex: 1,
    position: 'absolute',
  },
  containerStyle: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: 16,
  },
});

export default class Lsm extends Component {
  constructor(props) {
    super(props);

    this.ref = firebase.firestore().collection('lsm');
    this.state = {
      data: [],
      loading: true,
      active: 0,
    };
  }

  componentDidMount() {
    this.getData();
  }


  getData = () => {
    this.ref.orderBy('name', 'asc').get()
      .then(async (item) => {
        const tempData = [];

        await item.forEach((docs) => {
          const tempItem = docs.data();

          tempItem.key = docs.id;

          tempData.push(tempItem);
        });

        this.setState({ data: tempData, loading: false });
      });
  }

  renderer = ({ item }) => (
    <LsmItem
      name={item.name}
      address={item.address}
      telp={item.telp}
      fax={item.fax}
      hotline={item.hotline}
      email={item.email}
      website={item.website}
    />
  )

  render() {
    const { data, loading, active } = this.state;

    return (
      <Container>
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{ height }}
        >
          <View style={{ flex: 1 }}>
            <BackButton iconStyle={styles.backStyle} />
            <Carousel
              data={data}
              renderItem={this.renderer}
              sliderWidth={width}
              itemWidth={width}
              slideStyle={{ width }}
              inactiveSlideOpacity={1}
              inactiveSlideScale={1}
              lockScrollWhileSnapping
              loop
              autoplay
              autoplayDelay={2000}
              autoplayInterval={3000}
              onSnapToItem={(index) => { this.setState({ active: index }); }}
            />
            <Pagination
              dotsLength={data.length}
              activeDotIndex={active}
              containerStyle={styles.containerStyle}
            />
          </View>

        </LinearGradient>
        <Loading isLoading={loading} />
      </Container>
    );
  }
}
