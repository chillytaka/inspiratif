import React, { Component } from 'react';
import {
  Card, CardItem, Text,
} from 'native-base';
import {
  Linking, StyleSheet, View, Dimensions,
} from 'react-native';
import PropTypes from 'prop-types';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import colors from '../val/colors';
import Logo from '../reusable/Logo';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  cardStyle: {
    backgroundColor: colors.transparent,
  },
  textStyle: {
  },
  linkStyle: {
    fontWeight: 'bold',
  },
});


export default class LsmItem extends Component {
    static propTypes = {
      name: PropTypes.string,
      address: PropTypes.string,
      telp: PropTypes.string,
      fax: PropTypes.string,
      hotline: PropTypes.string,
      email: PropTypes.string,
      website: PropTypes.string,
    }

    static defaultProps = {
      name: undefined,
      address: undefined,
      telp: undefined,
      fax: undefined,
      hotline: undefined,
      email: undefined,
      website: undefined,
    }

    openLink = (url) => {
      Linking.openURL(url);
    }

    openEmail = (email) => {
      Linking.openURL(`mailto:${email}`);
    }

    openTelp = (telp) => {
      Linking.openURL(`tel://${telp}`);
    }

    render() {
      const {
        name,
        address,
        telp,
        fax,
        hotline,
        email,
        website,
      } = this.props;

      return (
        <View style={[{ height, width, padding: 16 }, styles.cardStyle]}>
          <Logo />
          <CardItem style={styles.cardStyle} header>
            <Text style={styles.textStyle}>{name}</Text>
          </CardItem>
          <CardItem style={styles.cardStyle}>
            <Text style={styles.textStyle}>{address}</Text>
          </CardItem>
          {telp
            ? (
              <CardItem style={styles.cardStyle}>
                <Text style={styles.textStyle}>Telp: </Text>
                <TouchableWithoutFeedback onPress={() => this.openTelp(telp)}>
                  <Text style={styles.linkStyle}>{telp}</Text>
                </TouchableWithoutFeedback>
              </CardItem>
            )
            : null}
          {fax
            ? (
              <CardItem style={styles.cardStyle}>
                <Text style={styles.textStyle}>{`fax: ${fax}`}</Text>
              </CardItem>
            )
            : null}
          {hotline
            ? (
              <CardItem style={styles.cardStyle}>
                <Text style={styles.textStyle}>{`hotline: ${hotline}`}</Text>
              </CardItem>
            )
            : null}
          {email
            ? (
              <CardItem style={styles.cardStyle}>
                <Text style={styles.textStyle}>Email: </Text>
                <TouchableWithoutFeedback onPress={() => this.openEmail(email)}>
                  <Text style={styles.linkStyle}>{email}</Text>
                </TouchableWithoutFeedback>
              </CardItem>
            )
            : null}
          {website
            ? (
              <CardItem style={styles.cardStyle}>
                <Text style={styles.textStyle}>Website: </Text>
                <TouchableWithoutFeedback onPress={() => this.openLink(website)}>
                  <Text style={styles.linkStyle}>{website}</Text>
                </TouchableWithoutFeedback>
              </CardItem>
            )
            : null}
        </View>
      );
    }
}
