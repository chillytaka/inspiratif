import React, { Component } from 'react';
import { GiftedChat } from 'react-native-gifted-chat';
import firebase from 'react-native-firebase';
import { Container } from 'native-base';
import { View } from 'react-native';
import Loading from '../reusable/Loading';
import Header from './Header';

export default class Chat extends Component {
  constructor(props) {
    super(props);

    this.refUser = firebase.firestore().collection('users');
    this.refMessage = firebase.firestore().collection('messages');
    this.refChat = null;
    this.srcUnsubscribe = null;
    this.state = {
      messages: [],
      userData: {},
      isLoading: true,
    };
  }

  async componentDidMount() {
    this.subscribeChat(await this.getUser());
  }

  componentWillUnmount() {
    this.srcUnsubscribe();
  }

  subscribeChat = async (userId) => {
    const { navigation } = this.props;
    const { id } = navigation.state.params;

    // subscribing
    await this.refMessage
      .where(`participant.${id}.id`, '==', id)
      .where(`participant.${userId}.id`, '==', userId)
      .get().then((item) => {
        if (item.docs[0] !== undefined) {
          this.refChat = item.docs[0].ref.collection('chat');
          this.srcUnsubscribe = this.refChat
            .orderBy('createdAt', 'asc').onSnapshot(this.onMessageUpdate);
        }
      });
  }

  // get username from firebase
  getUser = () => {
    const { uid } = firebase.auth().currentUser;
    // get user information from firestore based on logged in user id
    this.refUser.where('userId', '==', uid).get()
      .then((data) => {
        const docs = data.docs[0];
        const { username } = docs.data();

        const tempUser = {
          _id: uid,
          name: username,
        };

        this.setState({ userData: tempUser, isLoading: false });
      });
    return uid;
  }

  // update message state
  onMessageUpdate = async (snapshot) => {
    const { messages } = this.state;
    const tempData = [];
    const { docChanges } = snapshot;

    await docChanges.forEach((change) => {
      const {
        createdAt, text, sender, senderName,
      } = change.doc.data();

      tempData.unshift({
        _id: change.doc.id,
        text,
        createdAt: new Date(createdAt.toDate()),
        user: {
          _id: sender,
          name: senderName,
        },
      });
      return null;
    });

    const tempMessages = [...tempData, ...messages];
    this.setState({ messages: tempMessages });
  }

  // on User Send
  onSend = (messages = []) => {
    const { text, user, createdAt } = messages[0];
    const { navigation } = this.props;
    const { id, title } = navigation.state.params;

    if (this.refChat === null) {
      const tempParticipant = {
        [id]: {
          id,
          name: title,
        },
        [user._id]: {
          id: user._id,
          name: user.name,
        },
      };

      // create message document
      this.refMessage.add({
        participant: tempParticipant,
        user: [id, user._id],
      })
        .then(async () => {
          await this.subscribeChat(user._id); // subscribe to chat
          this.onSend(messages); // re-sending chat
        });
    } else {
      this.refChat.add({
        createdAt,
        text,
        sender: user._id,
        senderName: user.name,
      });
    }
  }

  render() {
    const { messages, userData, isLoading } = this.state;
    const { navigation } = this.props;
    const { title } = navigation.state.params;

    if (!isLoading) {
      return (
        <Container style={{ flex: 1 }}>
          <Header title={title} />

          <View style={{ flex: 1 }}>
            <GiftedChat
              messages={messages}
              user={userData}
              renderUsernameOnMessage
              onSend={this.onSend}
              inverted
            />
          </View>

        </Container>
      );
    } return (
      <Loading isLoading={isLoading} />
    );
  }
}
