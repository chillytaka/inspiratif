import React, { Component } from 'react';
import { FlatList, StatusBar } from 'react-native';
import firebase from 'react-native-firebase';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import BoxButton from '../reusable/BoxButton';
import BackButton from '../reusable/BackButton';
import colors from '../val/colors';
import Loading from '../reusable/Loading';

export default class DisplayUser extends Component {
  constructor(props) {
    super(props);

    this.refUser = firebase.firestore().collection('users');
    this.refMessage = firebase.firestore().collection('messages');
    this.state = {
      user: [],
      loading: true,
    };
  }

  async componentDidMount() {
    const psikolog = await this.checkUser();
    if (!psikolog) {
      this.getPsikolog();
    } else {
      this.getChattedUser();
    }
  }

  // check whether user is psikolog
  checkUser = () => {
    const { uid } = firebase.auth().currentUser;
    return this.refUser.where('userId', '==', uid).get()
      .then((data) => {
        const { psikolog, username } = data.docs[0].data();
        firebase.auth().currentUser.updateProfile({
          displayName: username,
        });
        return psikolog;
      });
  }

  // get psikolog
  getPsikolog = () => {
    this.refUser.where('psikolog', '==', true).get()
      .then(async (item) => {
        const tempData = [];
        await item.forEach((docs) => {
          const { username, userId } = docs.data();

          const tempItem = {
            key: docs.id, username, userId,
          };

          tempData.push(tempItem);
        });

        this.setState({ user: tempData, loading: false });
      });
  }

  // get chatted user
  getChattedUser =() => {
    const { uid, displayName } = firebase.auth().currentUser;

    this.refMessage.where(`participant.${uid}.id`, '==', uid)
      .get().then((item) => {
        const tempData = [];
        item.forEach(async (docs) => {
          const { participant, user } = docs.data();

          await user.map((idUser) => {
            // get the other participant name
            if (participant[idUser].name !== displayName) {
              const tempItem = {
                key: docs.id,
                username: participant[idUser].name,
                userId: participant[idUser].id,
              };

              tempData.push(tempItem);
            }
            return null;
          });

          this.setState({ user: tempData, loading: false });
        });
      });
  }

  onPress = (title, id) => {
    const { navigation } = this.props;
    navigation.navigate('Chat', { title, id });
  }

  renderer = ({ item }) => (
    <BoxButton
      title={item.username}
      idUser={item.userId}
      onPress={this.onPress}
    />
  )

  render() {
    const { loading, user } = this.state;
    return (
      <Container>
        <StatusBar hidden />
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{ height: '100%', padding: 16 }}
        >
          <BackButton />

          <FlatList data={user} renderItem={this.renderer} />
        </LinearGradient>
        <Loading isLoading={loading} />
      </Container>
    );
  }
}
