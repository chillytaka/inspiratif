import React, { Component } from 'react';
import {
  TouchableOpacity, StyleSheet, StatusBar,
} from 'react-native';
import {
  Header, Title, Icon, Left, Body, Right,
} from 'native-base';
import { withNavigation } from 'react-navigation';
import PropTypes from 'prop-types';
import colors from '../val/colors';

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: colors.white,
  },
});

class Atas extends Component {
    static propTypes = {
      title: PropTypes.string,
    }

    static defaultProps = {
      title: '',
    }

    render() {
      const { title, navigation } = this.props;
      return (
        <Header style={styles.headerStyle}>

          <StatusBar
            backgroundColor="white"
            barStyle="dark-content"
            hidden={false}
          />

          <Left>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Icon name="arrow-back" />
            </TouchableOpacity>
          </Left>

          <Body>
            <Title style={{ color: 'black' }}>{title}</Title>
          </Body>

          <Right />
        </Header>
      );
    }
}

export default withNavigation(Atas);
