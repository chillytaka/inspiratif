import React from 'react';
import { Text, View } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { TouchableWithoutFeedback, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import Logo from '../reusable/Logo';
import BackButton from '../reusable/BackButton';
import colors from '../val/colors';

const { width, height } = Dimensions.get('window');

const ApaNarkoba = ({ onPress }) => (
  <TouchableWithoutFeedback
    onPress={onPress}
    style={{ width, height }}
  >

    <LinearGradient
      colors={[colors.main1, colors.main2]}
      style={{ height, width, padding: 16 }}
    >
      <BackButton />

      <Logo />

      <View style={{ height: '10%' }} />

      <Text style={{ fontSize: 80, color: colors.darkgrey }}>Apa</Text>
      <Text style={{ fontSize: 80, color: colors.darkgrey }}>Itu</Text>
      <Text style={{ fontSize: 80, color: colors.darkgrey }}>Narkoba?</Text>
    </LinearGradient>
  </TouchableWithoutFeedback>
);

ApaNarkoba.propTypes = {
  onPress: PropTypes.func,
};

ApaNarkoba.defaultProps = {
  onPress: () => {},
};

export default ApaNarkoba;
