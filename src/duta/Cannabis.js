import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { Dimensions, Text } from 'react-native';
import { material } from 'react-native-typography';
import Logo from '../reusable/Logo';
import BackButton from '../reusable/BackButton';
import colors from '../val/colors';
import CustomButton from '../reusable/CustomButton';

const { width, height } = Dimensions.get('window');

const cannabis = require('../../assets/DaN/ganja.jpg');
const hashish = require('../../assets/DaN/hashish.jpg');

const Cannabis = ({ navigation }) => (
  <LinearGradient
    colors={[colors.main1, colors.main2]}
    style={{ height, width, padding: 16 }}
  >
    <BackButton />

    <Logo />

    <Text style={[material.display1, { textAlign: 'center', marginTop: 10 }]}>Cannabis</Text>

    <CustomButton text="Ganja" onPress={() => navigation.navigate('Photo', { photo: cannabis })} />
    <CustomButton text="Hashish" onPress={() => navigation.navigate('Photo', { photo: hashish })} />
  </LinearGradient>
);

export default Cannabis;
