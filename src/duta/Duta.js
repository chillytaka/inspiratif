import React, { useRef } from 'react';
import { ScrollView, View, Dimensions } from 'react-native';
import ApaNarkoba from './ApaNarkoba';
import Photo from './Photo';
import MainList from './MainList';

const photo2 = require('../../assets/DaN/2.jpg');

const { width } = Dimensions.get('window');
let page = 1;

const Duta = ({ navigation }) => {
  const scrollView = useRef(undefined);

  const onPress = () => {
    scrollView.current.scrollTo({ x: width * page, y: 0, animated: true });
    page += 1;
  };

  return (
    <View style={{ flex: 1 }}>
      <ScrollView
        horizontal
        pagingEnabled
        scrollEnabled={false}
        showsHorizontalScrollIndicator={false}
        ref={scrollView}
      >
        <ApaNarkoba onPress={onPress} />
        <Photo photo={photo2} onPress={onPress} />
        <MainList navigation={navigation} />
      </ScrollView>
    </View>
  );
};

export default Duta;
