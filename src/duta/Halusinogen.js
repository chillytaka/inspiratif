import React, { useRef } from 'react';
import { ScrollView, View, Dimensions } from 'react-native';
import Photo from './Photo';


const { width } = Dimensions.get('window');
let page = 1;
const photo = require('../../assets/DaN/awas.jpg');
const photo2 = require('../../assets/DaN/halusinogen.jpg');

const Halusinogen = ({ navigation }) => {
  const scrollView = useRef(undefined);

  const onPress = () => {
    scrollView.current.scrollTo({ x: width * page, y: 0, animated: true });
    page += 1;
  };

  return (
    <View style={{ flex: 1 }}>
      <ScrollView
        horizontal
        pagingEnabled
        scrollEnabled={false}
        showsHorizontalScrollIndicator={false}
        ref={scrollView}
      >
        <Photo photo={photo} navigation={navigation} onPress={onPress} />
        <Photo photo={photo2} navigation={navigation} />
      </ScrollView>
    </View>
  );
};

export default Halusinogen;
