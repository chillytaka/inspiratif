import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { Container } from 'native-base';
import { Dimensions } from 'react-native';
import Logo from '../reusable/Logo';
import BackButton from '../reusable/BackButton';
import colors from '../val/colors';
import CustomButton from '../reusable/CustomButton';

const { width, height } = Dimensions.get('window');

const MainList = ({ navigation }) => (
  <Container>

    <LinearGradient
      colors={[colors.main1, colors.main2]}
      style={{ height, width, padding: 16 }}
    >
      <BackButton />

      <Logo />

      <CustomButton text="Cannabis" onPress={() => navigation.navigate('Cannabis')} />
      <CustomButton text="Opiat" onPress={() => navigation.navigate('Opiat')} />
      <CustomButton text="ATS" onPress={() => navigation.navigate('Ats')} />
      <CustomButton text="Halusinogen" onPress={() => navigation.navigate('Halusinogen')} />
      <CustomButton text="Zat Adiktif" onPress={() => navigation.navigate('Adiktif')} />

    </LinearGradient>
  </Container>
);

export default MainList;
