import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { Container } from 'native-base';
import { Dimensions, Text } from 'react-native';
import { material } from 'react-native-typography';
import Logo from '../reusable/Logo';
import BackButton from '../reusable/BackButton';
import colors from '../val/colors';
import CustomButton from '../reusable/CustomButton';

const { width, height } = Dimensions.get('window');

const kokain = require('../../assets/DaN/ats.jpg');
const heroin = require('../../assets/DaN/heroin.jpg');
const morfin = require('../../assets/DaN/morfin.jpg');
const opiat = require('../../assets/DaN/opiat.jpg');

const Opiat = ({ navigation }) => (
  <Container>
    <LinearGradient
      colors={[colors.main1, colors.main2]}
      style={{ height, width, padding: 16 }}
    >
      <BackButton />

      <Logo />

      <Text style={[material.display1, { textAlign: 'center', marginTop: 10 }]}>Opiat</Text>

      <CustomButton text="Kokain" onPress={() => navigation.navigate('Photo', { photo: kokain })} />
      <CustomButton text="Heroin" onPress={() => navigation.navigate('Photo', { photo: heroin })} />
      <CustomButton text="Morfin" onPress={() => navigation.navigate('Photo', { photo: morfin })} />
      <CustomButton text="Opiat" onPress={() => navigation.navigate('Photo', { photo: opiat })} />
    </LinearGradient>
  </Container>
);

export default Opiat;
