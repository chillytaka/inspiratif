import React from 'react';
import {
  TouchableWithoutFeedback, Image, Dimensions,
} from 'react-native';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../val/colors';
import BackButton from '../reusable/BackButton';


const { width, height } = Dimensions.get('window');

const Photo = ({ photo, navigation, onPress }) => {
  let currentPhoto = photo;
  if (currentPhoto === undefined) {
    currentPhoto = navigation.state.params.photo;
  }

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <LinearGradient
        colors={[colors.main1, colors.main2]}
        style={{ height, width }}
      >
        <Image
          source={currentPhoto}
          style={{
            width: width - 30,
            height: height - 30,
            resizeMode: 'stretch',
            alignSelf: 'center',
          }}
        />
        <BackButton buttonStyle={{
          position: 'absolute', left: 16, top: 16,
        }}
        />
      </LinearGradient>
    </TouchableWithoutFeedback>
  );
};

Photo.propTypes = {
  photo: PropTypes.number,
  onPress: PropTypes.func,
};

Photo.defaultProps = {
  photo: undefined,
  onPress: () => {},
};

export default Photo;
