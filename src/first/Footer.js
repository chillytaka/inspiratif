import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import {
  Footer, FooterTab, Button, Text,
} from 'native-base';
import PropTypes from 'prop-types';
import colors from '../val/colors';


const styles = StyleSheet.create({
  background: {
    backgroundColor: colors.main2,
  },
  textStyle: {
    color: 'black',
  },
});


export default class Bawah extends Component {
  static propTypes = {
    onNext: PropTypes.func.isRequired,
    onPrev: PropTypes.func.isRequired,
    onSkip: PropTypes.func.isRequired,
  }

  render() {
    const { onNext, onPrev, onSkip } = this.props;
    return (
      <Footer style={styles.background}>
        <FooterTab>
          <Button style={styles.background} onPress={onPrev}>
            <Text style={styles.textStyle}>Prev</Text>
          </Button>
        </FooterTab>
        <FooterTab>
          <Button style={styles.background} onPress={onSkip}>
            <Text style={styles.textStyle}>Skip</Text>
          </Button>
        </FooterTab>
        <FooterTab>
          <Button style={styles.background} onPress={onNext}>
            <Text style={styles.textStyle}>Next</Text>
          </Button>
        </FooterTab>

      </Footer>
    );
  }
}
