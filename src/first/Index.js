/* eslint-disable global-require */
import React, { Component } from 'react';
import {
  View, StyleSheet, Dimensions,
} from 'react-native';
import {
  Container,
} from 'native-base';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../val/colors';
import Footer from './Footer';
import Item from './Item';

const styles = StyleSheet.create({
  viewLine: {
    backgroundColor: colors.white,
    height: 2,
    width: '100%',
  },
});

const data = [
  { title: 'Mari Bicara Dengan Kami !', img: require('../../assets/page1.png') },
  { title: 'Yuk Menenangkan Diri Sejenak !', img: require('../../assets/first/tenang.jpeg') },
  { title: 'Hari Ini Penuh Berkah !', img: require('../../assets/first/metime.jpeg') },
  { title: 'Harta Sejati Adalah Kesehatan !', img: require('../../assets/first/stamina.jpeg') },
  { title: 'Hari Ini Harus Lebih Baik !', img: require('../../assets/first/semangat.jpeg') },
  { title: 'Yuk Beraktifitas !', img: require('../../assets/first/sehat.jpeg') },
];

const { width, height } = Dimensions.get('window');

export default class Index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: 0,
    };
  }

renderer = ({ item }) => (
  <Item title={item.title} img={item.img} />
)

// next button Pressed
onNext = () => {
  const { currentIndex } = this._carousel;
  if (currentIndex + 1 === data.length) {
    this.onSkip();
  } else {
    this._carousel.snapToNext();
  }
}

// prev button Pressed
onPrev = () => {
  this._carousel.snapToPrev();
}

onSkip = () => {
  const { navigation } = this.props;
  navigation.navigate('mainNav');
}

render() {
  const { active } = this.state;
  return (
    <Container>
      <LinearGradient
        colors={[colors.main1, colors.main2]}
        style={{ height, width, flex: 1 }}
      >
        <View style={{ flex: 1 }}>
          <Carousel
            data={data}
            renderItem={this.renderer}
            ref={(c) => { this._carousel = c; }}
            sliderWidth={width}
            itemWidth={width}
            slideStyle={{ width }}
            inactiveSlideOpacity={1}
            inactiveSlideScale={1}
            onSnapToItem={(index) => { this.setState({ active: index }); }}
          />
          <Pagination
            dotsLength={data.length}
            activeDotIndex={active}
            containerStyle={{ backgroundColor: colors.transparent }}
          />
        </View>
        <View style={styles.viewLine} />
        <Footer
          onPrev={this.onPrev}
          onNext={this.onNext}
          onSkip={this.onSkip}
        />
      </LinearGradient>
    </Container>
  );
}
}
