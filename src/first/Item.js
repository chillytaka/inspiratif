import React, { Component } from 'react';
import {
  Text, StyleSheet, Image, Dimensions,
} from 'react-native';
import { material } from 'react-native-typography';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../val/colors';
import logo from '../val/logo';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  titleText: {
    textAlign: 'center',
  },
  logoStyle: {
    width: '50%',
    height: '20%',
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  imageStyle: {
    width: width / 2,
    height: width / 2,
    borderRadius: width / 2,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: '10%',
  },
});

export default class Item extends Component {
    static propTypes = {
      title: PropTypes.string.isRequired,
      img: PropTypes.number.isRequired,
    }

    render() {
      const { title, img } = this.props;
      return (
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{ height, width, padding: 16 }}
        >
          <Image source={logo} style={styles.logoStyle} />
          <Text style={[material.title, styles.titleText]}>{title}</Text>
          <Image source={img} style={styles.imageStyle} />
        </LinearGradient>
      );
    }
}
