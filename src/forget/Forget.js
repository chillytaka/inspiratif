import React, { Component } from 'react';
import {
  StatusBar, StyleSheet, View,
} from 'react-native';
import {
  Container, Text, Button, Card, Item, Input,
} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { material } from 'react-native-typography';
import firebase from 'react-native-firebase';
import colors from '../val/colors';
import TextButton from '../reusable/TextButton';
import Error from '../reusable/ErrorToast';

const styles = StyleSheet.create({
  resetText: {
    alignSelf: 'center',
    marginTop: '40%',
  },
  resetButton: {
    backgroundColor: colors.accent,
    alignSelf: 'center',
    marginTop: 20,
  },
  separator: {
    height: '10%',
  },
});

export default class Forget extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
    };
  }

  changeText = (text) => {
    this.setState({ email: text });
  }

  // sent reset password email
  onReset = () => {
    const { email } = this.state;
    firebase.auth().sendPasswordResetEmail(email)
      .then(() => { Error('please check your email to reset the password'); })
      .catch(error => Error(error.message));
  }

  backToLogin = () => {
    const { navigation } = this.props;
    navigation.navigate('Login');
  }

  render() {
    return (
      <Container>
        <StatusBar hidden />
        <LinearGradient
          colors={[colors.main, colors.grey]}
          style={{ height: '100%' }}
        >
          <Text style={[styles.resetText, material.display2]}>Reset Passwords</Text>

          <View style={styles.separator} />
          <Card style={{ width: '90%', alignSelf: 'center' }}>
            <Item>
              <Input
                placeholder="Email"
                onChangeText={text => this.changeText(text)}
              />
            </Item>
          </Card>

          <Button
            style={styles.resetButton}
            rounded
            onPress={this.onReset}
          >
            <Text>Reset</Text>
          </Button>

          <View style={styles.separator} />
          <TextButton text="Back to Login" onPress={this.backToLogin} />
        </LinearGradient>
      </Container>
    );
  }
}
