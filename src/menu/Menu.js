import React, { Component } from 'react';
import {
  Text, View, TouchableOpacity, StyleSheet, ImageBackground, Image, StatusBar,
} from 'react-native';
import { material } from 'react-native-typography';
import { Fab, Icon } from 'native-base';
import firebase from 'react-native-firebase';
import colors from '../val/colors';
import logo from '../val/logo';

const background = require('../../assets/bg.jpg');

const styles = StyleSheet.create({
  menu1: {
    backgroundColor: colors.main1,
    height: '40%',
    elevation: 15,
  },
  textStyle: {
    position: 'absolute',
    bottom: 0,
    margin: 16,
  },
  menu2: {
    backgroundColor: colors.inbetween1,
    height: '12%',
    width: '90%',
    elevation: 10,
  },
  menu3: {
    backgroundColor: colors.inbetween2,
    height: '12%',
    width: '80%',
    elevation: 8,
  },
  menu4: {
    backgroundColor: colors.inbetween3,
    height: '12%',
    width: '70%',
    elevation: 5,
  },
  menu5: {
    backgroundColor: colors.inbetween4,
    height: '12%',
    width: '60%',
    elevation: 3,
  },
  menu6: {
    backgroundColor: colors.inbetween6,
    height: '12%',
    width: '50%',
  },
  imageStyle: {
    width: '90%',
    height: '60%',
    alignSelf: 'center',
    resizeMode: 'contain',
    marginTop: '5%',
  },
});


export default class Menu extends Component {
  onLogout = () => {
    const { navigation } = this.props;
    firebase.auth().signOut();
    navigation.navigate('Welcome');
  }

  onScreen =(screen) => {
    const { navigation } = this.props;
    navigation.navigate(screen);
  }

  render() {
    return (
      <View>
        <StatusBar hidden={false} />
        <ImageBackground
          source={background}
          style={{ width: '100%', height: '100%' }}
        >
          <TouchableOpacity
            style={styles.menu1}
            onPress={() => this.onScreen('Bicara')}
          >
            <Image
              source={logo}
              style={styles.imageStyle}
            />
            <Text style={[styles.textStyle, material.title]}>Aku Butuh Bicara</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menu2}
            onPress={() => this.onScreen('Tenang')}
          >
            <Text style={[material.title, styles.textStyle]}>Aku Butuh Tenang</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menu3}
            onPress={() => this.onScreen('Semangat')}
          >
            <Text style={[material.title, styles.textStyle]}>Aku Butuh Semangat</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menu4}
            onPress={() => this.onScreen('Sehat')}
          >
            <Text style={[material.title, styles.textStyle]}>Aku Butuh Sehat</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menu5}
            onPress={() => this.onScreen('Metime')}
          >
            <Text style={[material.title, styles.textStyle]}>Aku Butuh &quot;Me Time&quot;</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menu6}
            onPress={() => this.onScreen('Stamina')}
          >
            <Text style={[material.title, styles.textStyle]}>Aku Butuh Stamina</Text>
          </TouchableOpacity>
          <Fab style={{ backgroundColor: colors.red }} onPress={this.onLogout}>
            <Icon name="log-out" />
          </Fab>
        </ImageBackground>
      </View>
    );
  }
}
