import React, { Component } from 'react';
import { CheckBox } from 'react-native-btr';
import {
  ListItem, Text, Left, Body, Right,
} from 'native-base';
import PropTypes from 'prop-types';


export default class CheckItem extends Component {
    static propTypes = {
      text: PropTypes.string.isRequired,
      checked: PropTypes.bool,
      onCheck: PropTypes.func,
      id: PropTypes.number.isRequired,
    }

    static defaultProps = {
      checked: false,
      onCheck: () => {},
    }

    render() {
      const {
        checked, text, onCheck, id,
      } = this.props;
      return (
        <ListItem>
          <Left>
            <CheckBox
              checked={checked}
              onPress={() => onCheck(id)}
            />
          </Left>
          <Body>
            <Text>{text}</Text>
          </Body>
          <Right />
        </ListItem>
      );
    }
}
