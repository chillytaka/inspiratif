import React, { Component } from 'react';
import {
  View, StyleSheet, Dimensions,
} from 'react-native';
import { Text, Button } from 'native-base';
import { RadioGroup } from 'react-native-btr';
import { material } from 'react-native-typography';
import SimpleCheck from './SimpleCheck';
import colors from '../val/colors';
import ResultFirst from './ResultFirst';

const styles = StyleSheet.create({
  separator: { height: '5%' },
  buttonStyle: {
    alignSelf: 'center',
    backgroundColor: colors.accent,
  },
  radioStyle: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 15,
  },
  viewStyle: {
    marginTop: 10,
    height: 70,
  },
});

const { width } = Dimensions.get('window');

export default class FirstPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      olahraga: [
        { label: '<10mnt', flexDirection: 'row' },
        { label: '>10mnt', flexDirection: 'row' },
      ],
      salam: [
        { label: '<5', flexDirection: 'row' },
        { label: '>5', flexDirection: 'row' },
      ],
      hati: [
        { label: 'Tidak Menentu', flexDirection: 'row', checked: true },
        { label: 'Bahagia', flexDirection: 'row' },
        { label: 'Sedih', flexDirection: 'row' },
      ],
      total: 0,
      stateHati: 0,
      finishModal: false,
      currentTotal: 0,
    };
  }

   changeOR = (olahraga) => {
     this.setState({ olahraga });
   }

   changeSalam = (salam) => {
     this.setState({ salam });
   }

   changeHati = (hati) => {
     this.setState({ hati });
   }

   onDzikirPress = (status) => {
     if (status) {
       this.setState(prev => ({ total: prev.total + 1 }));
     } else {
       this.setState(prev => ({ total: prev.total - 1 }));
     }
   }

   onFinish = () => {
     const {
       total, salam, olahraga, hati,
     } = this.state;
     let tempTotal = total;

     for (let i = 0; i !== salam.length; i += 1) {
       if (salam[i].checked) {
         tempTotal += 1;
         break;
       }
     }

     for (let i = 0; i !== olahraga.length; i += 1) {
       if (olahraga[i].checked) {
         tempTotal += 1;
         break;
       }
     }

     for (let i = 0; i !== hati.length; i += 1) {
       if (hati[i].checked) {
         this.setState({ stateHati: i });
         break;
       }
     }


     this.setState({ currentTotal: tempTotal, finishModal: true });
   }

   toggleVisible =() => {
     this.setState(prev => ({ finishModal: !prev.finishModal, currentTotal: 0 }));
   }

   render() {
     const {
       salam, olahraga, hati, currentTotal, stateHati, finishModal,
     } = this.state;
     return (
       <View style={{ width: width - 33 }}>
         <Text>Bagaimana Kondisi Psikologis Anda ?</Text>
         <View style={styles.viewStyle}>
           <Text style={[material.body2, { alignSelf: 'center' }]}>
          Sudahkah Anda Berdzikir Hari Ini ?
           </Text>
           <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
             <SimpleCheck text="Pagi" onCheck={this.onDzikirPress} />
             <SimpleCheck text="Sore" onCheck={this.onDzikirPress} />
           </View>
         </View>
         <View style={styles.viewStyle}>
           <Text style={[material.body2, { alignSelf: 'center' }]}>
          Sudahkah Anda Berolahraga ?
           </Text>
           <RadioGroup
             radioButtons={olahraga}
             style={{ flexDirection: 'row', alignSelf: 'center' }}
             labelStyle={material.body1}
             onPress={this.changeOR}
           />
         </View>
         <View style={styles.viewStyle}>
           <Text style={[material.body2, { alignSelf: 'center', textAlign: 'center' }]}>
          Berapa Salam Yang Anda Berikan Kepada Orang Di Sekitar Anda ?
           </Text>
           <RadioGroup
             radioButtons={salam}
             style={{ flexDirection: 'row', alignSelf: 'center' }}
             labelStyle={material.body1}
             onPress={this.changeSalam}
           />
         </View>
         <View style={styles.viewStyle}>
           <Text style={[material.body2, { alignSelf: 'center', textAlign: 'center' }]}>
          Bagaimana Suasana Hati Anda Hari Ini ?
           </Text>
           <RadioGroup
             radioButtons={hati}
             style={{ flexDirection: 'row', alignSelf: 'center' }}
             labelStyle={material.body1}
             onPress={this.changeHati}
           />
         </View>
         <Button
           rounded
           style={styles.buttonStyle}
           onPress={this.onFinish}
         >
           <Text>Finish</Text>
         </Button>

         <ResultFirst
           toggleVisible={this.toggleVisible}
           visible={finishModal}
           number={currentTotal}
           stateHati={stateHati}
         />
       </View>
     );
   }
}
