import React, { Component } from 'react';
import { Dimensions, ScrollView } from 'react-native';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import Title from '../reusable/Title';
import Logo from '../reusable/Logo';
import colors from '../val/colors';
import SecondPage from './SecondPage';
import FirstPage from './FirstPage';
import BackButton from '../reusable/BackButton';
import Web from './WebView';

const { height } = Dimensions.get('window');

export default class Metime extends Component {
  render() {
    return (
      <Container>
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{ height, padding: 16 }}
        >
          <BackButton />
          <Logo />


          <Title title="Aku Butuh &quot;Me Time&quot; !" />

          <ScrollView
            horizontal
            pagingEnabled
          >
            <Web />
            <FirstPage />
            <SecondPage />
          </ScrollView>
        </LinearGradient>
      </Container>
    );
  }
}
