import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import Modal from 'react-native-modal';
import { Card, CardItem, Icon } from 'native-base';
import PropTypes from 'prop-types';
import { material } from 'react-native-typography';

const styles = StyleSheet.create({
  textStyle: {
    textAlign: 'center',
  },
});


class ResultFirst extends Component {
    static propTypes ={
      number: PropTypes.number,
      visible: PropTypes.bool,
      toggleVisible: PropTypes.func.isRequired,
      stateHati: PropTypes.number,

    }

    static defaultProps = {
      number: 0,
      visible: false,
      stateHati: 0,
    }

    renderIcon = () => {
      const { number } = this.props;
      const tempRender = [];
      for (let i = 0; i !== number; i += 1) {
        tempRender.push(
          <Icon name="star" key={i} />,
        );
      }
      return tempRender;
    }

    renderResult = () => {
      const { stateHati } = this.props;
      if (stateHati === 0) {
        return (
          <CardItem style={{ alignSelf: 'center' }}>
            <Text style={[material.subheading, styles.textStyle]}>
              Kenapa? Yuk klik dan cerita ke aku butuh bicara ☺ Ada psikolog yang akan memperhatikan kamu
            </Text>
          </CardItem>
        );
      } if (stateHati === 1) {
        return (
          <CardItem style={{ alignSelf: 'center' }}>
            <Text style={[material.subheading, styles.textStyle]}>
            Alhamdulillah, Yuk Tingkatkan
            </Text>
          </CardItem>
        );
      }
      return (
        <CardItem style={{ alignSelf: 'center' }}>
          <Text style={[material.subheading, styles.textStyle]}>
            Jangan sedih, Allah bersamamu
          </Text>
        </CardItem>
      );
    }

    render() {
      const { visible, toggleVisible } = this.props;
      return (
        <Modal
          isVisible={visible}
          onBackButtonPress={toggleVisible}
          onBackdropPress={toggleVisible}
        >
          <Card>
            <CardItem style={{ alignSelf: 'center' }}>
              {this.renderIcon()}
            </CardItem>

            {this.renderResult()}
          </Card>
        </Modal>
      );
    }
}

export default ResultFirst;
