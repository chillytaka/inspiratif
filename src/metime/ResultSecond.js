import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import Modal from 'react-native-modal';
import { Card, CardItem, Icon } from 'native-base';
import PropTypes from 'prop-types';
import { material } from 'react-native-typography';

const styles = StyleSheet.create({
  textStyle: {
    textAlign: 'center',
  },
});


class ResultSecond extends Component {
    static propTypes ={
      number: PropTypes.number,
      visible: PropTypes.bool,
      toggleVisible: PropTypes.func.isRequired,

    }

    static defaultProps = {
      number: 0,
      visible: false,
    }

    renderIcon = () => {
      const { number } = this.props;
      const tempRender = [];
      for (let i = 0; i !== number; i += 1) {
        tempRender.push(
          <Icon name="sad" key={i} />,
        );
      }
      return tempRender;
    }

    renderResult = () => {
      const { number } = this.props;
      if (number > 0) {
        return (
          <CardItem style={{ alignSelf: 'center' }}>
            <Text style={[material.subheading, styles.textStyle]}>
            Sesungguhnya Sakit adalah Penggugur Dosa
            </Text>
          </CardItem>
        );
      }
      return (
        <CardItem style={{ alignSelf: 'center' }}>
          <Text style={[material.subheading, styles.textStyle]}>Alhamdulillah</Text>
        </CardItem>
      );
    }

    render() {
      const { visible, toggleVisible } = this.props;
      return (
        <Modal
          isVisible={visible}
          onBackButtonPress={toggleVisible}
          onBackdropPress={toggleVisible}
        >
          <Card>
            <CardItem style={{ alignSelf: 'center' }}>
              {this.renderIcon()}
            </CardItem>

            {this.renderResult()}
          </Card>
        </Modal>
      );
    }
}

export default ResultSecond;
