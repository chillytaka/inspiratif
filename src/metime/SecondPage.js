import React, { Component } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { Text, Button } from 'native-base';
import CheckItem from './CheckItem';
import colors from '../val/colors';
import ResultSecond from './ResultSecond';

const styles = StyleSheet.create({
  separator: { height: '10%' },
  buttonStyle: {
    alignSelf: 'center',
    backgroundColor: colors.accent,
  },
});

const { width } = Dimensions.get('window');

export default class SecondPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [
        { key: 0, text: 'Mata Memerah', checked: false },
        { key: 1, text: 'Mual Muntah', checked: false },
        { key: 2, text: 'Sakit Kepala', checked: false },
        { key: 3, text: 'Mudah Haus', checked: false },
        { key: 4, text: 'Pilek', checked: false },
      ],
      finishModal: false,
      number: 0,
    };
  }

  onCheck = (key) => {
    const { data } = this.state;
    const tempData = data;
    for (let i = 0; i !== tempData.length; i += 1) {
      if (tempData[i].key === key) {
        tempData[i].checked = !tempData[i].checked;
        break;
      }
    }

    this.setState({ data: tempData });
  };

  onFinish = () => {
    const { data } = this.state;

    const tempData = data;
    let tempTotal = 0;
    for (let i = 0; i !== tempData.length; i += 1) {
      if (tempData[i].checked) {
        tempTotal += 1;
      }
    }

    this.setState({ number: tempTotal, finishModal: true });
  }

  toggleVisible = () => {
    this.setState(prev => ({ finishModal: !prev.finishModal }));
  }

  render() {
    const { data, finishModal, number } = this.state;
    return (
      <View style={{ width: width - 16 }}>
        <Text>Bagaimana Kondisi Fisik Anda ?</Text>
        <View style={styles.separator} />
        {
            data.map(item => (
              <CheckItem
                text={item.text}
                checked={item.checked}
                key={item.key}
                id={item.key}
                onCheck={this.onCheck}
              />
            ))
        }
        <Button
          rounded
          style={styles.buttonStyle}
          onPress={this.onFinish}
        >
          <Text>Finish</Text>
        </Button>

        <ResultSecond visible={finishModal} toggleVisible={this.toggleVisible} number={number} />
      </View>
    );
  }
}
