import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { CheckBox } from 'react-native-btr';
import { material } from 'react-native-typography';
import PropTypes from 'prop-types';


export default class SimpleCheck extends Component {
    static propTypes = {
      text: PropTypes.string.isRequired,
      onCheck: PropTypes.func.isRequired,
    }

    constructor(props) {
      super(props);

      this.state = {
        check: false,
      };
    }

    onCheck = () => {
      const { onCheck } = this.props;
      const { check } = this.state;

      if (check) {
        onCheck(false);
      } else {
        onCheck(true);
      }

      this.setState(prev => ({ check: !prev.check }));
    }

    render() {
      const { text } = this.props;
      const { check } = this.state;
      return (
        <View style={{ flexDirection: 'row', margin: 5 }}>
          <CheckBox checked={check} onPress={this.onCheck} />
          <Text style={material.body1}>{text}</Text>
        </View>
      );
    }
}
