import React, { Component } from 'react';
import { WebView } from 'react-native-webview';
import firebase from 'react-native-firebase';
import { View, Dimensions, StyleSheet } from 'react-native';
import { Button, Text } from 'native-base';
import { material } from 'react-native-typography';
import colors from '../val/colors';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  buttonStyle: {
    alignSelf: 'center',
    backgroundColor: colors.accent,
    marginTop: 10
  }
});

export class Web extends Component {
  constructor(props) {
    super(props);

    this.state = {
      old: undefined,
      remaja: 'https://google.com',
      dewasa: ''
    };

    this.ref = firebase
      .firestore()
      .collection('metime')
      .doc('oHkhjh6HX5k7dQDHk2HO');
  }
  componentDidMount = () => {
    this.getData();
  };

  getData = () => {
    this.ref
      .get()
      .then(item => {
        const data = item.data();
        this.setState({ remaja: data.remaja, dewasa: data.dewasa });
      })
      .catch(error => console.log(error));
  };

  render() {
    const { old, remaja, dewasa } = this.state;
    if (old < 25) {
      return (
        <View style={{ width: width - 33 }}>
          <WebView source={{ uri: remaja }} />
        </View>
      );
    }
    if (old >= 25) {
      return (
        <View style={{ width: width - 33 }}>
          <WebView source={{ uri: dewasa }} />
        </View>
      );
    }
    return (
      <View style={{ width: width - 33 }}>
        <Text style={[material.headline, { textAlign: 'center' }]}>
          {' '}
          Umurku Adalah... ?
        </Text>
        <Button
          rounded
          style={styles.buttonStyle}
          onPress={() => this.setState({ old: 27 })}
        >
          <Text>Diatas 25 Tahun</Text>
        </Button>

        <Button
          rounded
          style={styles.buttonStyle}
          onPress={() => this.setState({ old: 2 })}
        >
          <Text>Dibawah 25 Tahun</Text>
        </Button>
      </View>
    );
  }
}

export default Web;
