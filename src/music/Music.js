import React, { Component } from 'react';
import {
  Text, StyleSheet, View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import TrackPlayer from 'react-native-track-player';
import { Container, Button, Icon } from 'native-base';
import colors from '../val/colors';
import BackButton from '../reusable/BackButton';
import NameDisplay from './NameDisplay';

const styles = StyleSheet.create({
  playStyle: {
    marginTop: '20%',
    color: colors.white,
  },
  iconStyle: {
    color: colors.white,
  },
  buttonStyle: {
    alignSelf: 'center',
    backgroundColor: colors.white,
    marginTop: 50,
  },
  viewContainer: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },

  changeStyle: {
    alignSelf: 'center',
    backgroundColor: colors.white,
    marginTop: 20,
  },
});

let currentPlay = 1;

export default class Music extends Component {
  constructor(props) {
    super(props);

    this.ayah = 0;
    this.state = {
      isPlaying: true,
      title: '',
    };
  }

  async componentDidMount() {
    const { navigation } = this.props;
    TrackPlayer.setupPlayer() // setup trackplayer
      .then(() => {
        TrackPlayer.updateOptions({ // setup trackplayer options
          stopWithApp: true,
          capabilities: [
            TrackPlayer.CAPABILITY_PLAY,
            TrackPlayer.CAPABILITY_PAUSE,
            TrackPlayer.CAPABILITY_STOP,
          ],
          compactCapabilities: [
            TrackPlayer.CAPABILITY_PLAY,
            TrackPlayer.CAPABILITY_PAUSE,
          ],
        });
        if (navigation.state.params.source === 'natural') {
          this.playNatural();
        } else if (navigation.state.params.source === 'murotal') {
          this.playMurotal();
        } else if (navigation.state.params.source === 'instrumental') {
          this.playInstrumental();
        }
      });
  }

  // play natural music
  playNatural = () => {
    this.setState({ title: 'Nature Sound' });
    TrackPlayer.add([{
      id: '2',
      url: 'http://209.133.216.3:7118/stream',
      title: 'Nature Sound - Costa Rica Area',
      artist: 'Live Nature',
      artwork: 'https://images.unsplash.com/photo-1502082553048-f009c37129b9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
    }, {
      id: '3',
      url: 'http://209.133.216.3:7162/stream',
      title: 'Nature Sound - Lake Travis',
      artist: 'Live Nature',
      artwork: 'https://images.unsplash.com/photo-1502082553048-f009c37129b9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
    }, {
      id: '4',
      url: 'http://209.133.216.3:7232/autodj',
      title: 'Nature Sound - Colorado River Refuge',
      artist: 'Live Nature',
      artwork: 'https://images.unsplash.com/photo-1502082553048-f009c37129b9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
    }]).then(() => TrackPlayer.play());
  }

  playMurotal = () => {
    const server = 'https://cdn.alquran.cloud/media/audio/ayah/ar.alafasy/333';
    this.setState({ title: 'Murottal' });
    TrackPlayer.add({
      id: '1',
      url: server,
      title: 'Murottal',
      artist: 'Alquran Cloud',
      artwork: 'https://images.unsplash.com/photo-1535552081922-631f7dea1e9d',
    });
  }

  playInstrumental = () => {
    this.setState({ title: 'Davide of MIMIC' });
    TrackPlayer.add({
      id: '5',
      url: 'http://uk3.internet-radio.com:8060/stream',
      title: 'Davide of MIMIC',
      artist: 'Davide of MIMIC',
      artwork: 'https://images.unsplash.com/photo-1520523839897-bd0b52f945a0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
    }).then(() => TrackPlayer.play());
  }

  playing = async () => {
    TrackPlayer.play();
    const status = await TrackPlayer.getState();
    this.setState({ isPlaying: status });
  }

  nextTrack = () => {
    if (currentPlay !== 3) {
      TrackPlayer.skipToNext();
      currentPlay += 1;
    }
  }

  prevTrack = () => {
    if (currentPlay !== 1) {
      TrackPlayer.skipToPrevious();
      currentPlay -= 1;
    }
  }

  pausing = async () => {
    TrackPlayer.pause();
    const status = await TrackPlayer.getState();
    this.setState({ isPlaying: status });
  }

  playButton = () => {
    const { isPlaying } = this.state;
    if (isPlaying === TrackPlayer.STATE_PAUSED) {
      return (
        <Button
          rounded
          style={styles.buttonStyle}
          onPress={this.playing}
        >
          <View style={styles.viewContainer}>
            <Icon name="play" style={{ color: 'black' }} />
            <Text>Play</Text>
          </View>
        </Button>
      );
    }
    return (
      <Button
        rounded
        style={styles.buttonStyle}
        onPress={this.pausing}
      >
        <View style={styles.viewContainer}>
          <Icon name="pause" style={{ color: 'black' }} />
          <Text>Pause</Text>
        </View>
      </Button>
    );
  }

  renderChangeTrack = () => {
    const { navigation } = this.props;
    if (navigation.state.params.source === 'natural') {
      return (
        <View style={styles.viewContainer}>

          <Button
            rounded
            style={styles.changeStyle}
            onPress={this.prevTrack}
          >
            <View style={styles.viewContainer}>
              <Icon name="skip-backward" style={{ color: 'black' }} />
              <Text>Prev Track</Text>
            </View>
          </Button>

          <View style={{ width: 20 }} />

          <Button
            rounded
            style={styles.changeStyle}
            onPress={this.nextTrack}
          >
            <View style={styles.viewContainer}>
              <Icon name="skip-forward" style={{ color: 'black' }} />
              <Text>Next Track</Text>
            </View>
          </Button>

        </View>
      );
    }
    return null;
  }

  render() {
    const { title } = this.state;
    return (
      <Container>
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{
            height: '100%', padding: 16, alignItems: 'center',
          }}
        >
          <BackButton />

          <View style={{ height: 20 }} />
          <NameDisplay musicName={title} />

          {this.renderChangeTrack()}

          {this.playButton()}

        </LinearGradient>
      </Container>
    );
  }
}
