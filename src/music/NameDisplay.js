import React, { Component } from 'react';
import {
  Text, Dimensions, View, StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import { human } from 'react-native-typography';
import colors from '../val/colors';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  borderCircle: {
    width: width - 120,
    height: width - 120,
    borderRadius: (width - 120) / 2,
    borderWidth: 3,
    borderColor: colors.white,
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
});

export default class NameDisplay extends Component {
    static propTypes = {
      musicName: PropTypes.string,
    }

    static defaultProps = {
      musicName: '',
    }

    render() {
      const { musicName } = this.props;
      return (
        <View style={styles.borderCircle}>
          <Text style={[{ textAlign: 'center' }, human.title2White]}>{musicName}</Text>
        </View>
      );
    }
}
