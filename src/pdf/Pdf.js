import React, { Component } from 'react';
import {
  View, StyleSheet, Dimensions,
} from 'react-native';
import { Fab, Icon } from 'native-base';
import Pdf from 'react-native-pdf';
import Loading from '../reusable/Loading';
import colors from '../val/colors';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  pdfStyle: {
    flex: 1,
  },
  container: {
    flex: 1,
    width,
  },
  icon: {
    backgroundColor: colors.accent,
  },
  backStyle: {
    margin: 16,
  },
});

const source = [
  { uri: 'bundle-assets://dzikir1.pdf' },
  { uri: 'bundle-assets://dzikir2.pdf' },
];

export default class PdfView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pdfType: 0,
    };
  }

    Load = () => (
      <Loading isLoading />
    )

    switchType = () => {
      const { pdfType } = this.state;
      if (pdfType === 0) { this.setState({ pdfType: 1 }); } else {
        this.setState({ pdfType: 0 });
      }
    }

    render() {
      const { pdfType } = this.state;
      return (
        <View style={styles.container}>
          <Pdf
            source={source[pdfType]}
            style={styles.pdfStyle}
          />
          <Fab style={styles.icon} onPress={this.switchType}>
            <Icon name="code-working" />
          </Fab>
        </View>
      );
    }
}
