import React, { Component } from 'react';
import {
  StatusBar, StyleSheet, View, ImageBackground, Dimensions,
} from 'react-native';
import {
  Container, Text, Icon,
} from 'native-base';
import { human } from 'react-native-typography';
import moment from 'moment';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ViewShot from 'react-native-view-shot';
import Share from 'react-native-share';
import Error from '../reusable/ErrorToast';
import colors from '../val/colors';
import getQuote from '../api/Forismatic';
import getPhoto from '../api/Unsplash';
import Loading from '../reusable/Loading';
import SwipeGesture from '../swipe-gesture/swipe-gesture';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  dateText: {
    marginTop: '50%',
  },
  quoteText: {
    marginTop: '10%',
  },
  viewStyle: {
    width: width - 30,
    height: width - 30,
    marginTop: 50,
    alignSelf: 'center',
    marginLeft: '100%',
  },
  authorText: {
    marginTop: '10%',
    fontStyle: 'italic',
  },
  iconStyle: {
    position: 'relative',
    left: '90%',
    top: 16,
  },
  bottomContainer: {
    width: 100,
    height: 100,
    borderRadius: 360,
    backgroundColor: colors.white,
    alignItems: 'center',
    position: 'absolute',
    top: height - 80,
    alignSelf: 'center',
  },
});


export default class Quote extends Component {
  constructor(props) {
    super(props);

    this.state = {
      date: '',
      month: '',
      quote: 'This is the most awesome quote',
      author: 'myself',
      img: 'https://images.unsplash.com/photo-1547458784-bdc75d9a6cd9?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjc0MjIzfQ',
      isLoading: true,
    };
  }

  componentDidMount = () => {
    getQuote()
      .then((res) => {
        getPhoto()
          .then((resPhoto) => {
            this.setState({
              img: resPhoto,
              quote: res.quoteText,
              author: res.quoteAuthor,
              isLoading: false,
            });
          });
      });
    this.setState({
      date: moment().format('D'),
      month: moment().format('MMM YYYY'),
    });
  }

  swipeFunc = (action) => {
    const { navigation } = this.props;
    if (action === 'up') {
      navigation.navigate('Menu');
    }
  }

  CaptureScreen = () => {
    this._screen.capture()
      .then(
        async (res) => {
          const shareOptions = {
            title: 'Share Photo',
            url: `${res}`,
            failsOnCancel: false,
          };

          try {
            const shareResponse = await Share.open(shareOptions);
            console.log(shareResponse);
          } catch (error) {
            Error(error);
          }
        },
        error => Error(error),
      );
  }

  render() {
    const {
      date, month, quote, author, isLoading, img,
    } = this.state;
    return (
      <Container ref={(c) => { this._main = c; }}>
        <StatusBar hidden />
        <ImageBackground
          source={{ uri: img }}
          style={{ height: '100%', width: '100%' }}
          blurRadius={0.6}
        >
          <TouchableOpacity style={styles.iconStyle} onPress={this.CaptureScreen}>
            <Icon name="share" style={{ color: colors.white }} />
          </TouchableOpacity>
          <SwipeGesture
            onSwipePerformed={this.swipeFunc}
            gestureStyle={{ width: '100%', height: '100%' }}
          >


            <View style={styles.container}>
              <Text style={[human.largeTitleWhite, styles.dateText]}>{date}</Text>
              <Text style={human.bodyWhite}>{month}</Text>
              <Text style={[human.title1White, styles.quoteText]}>{quote}</Text>
              <Text style={[human.calloutWhite, styles.authorText]}>{author}</Text>
            </View>

            <View style={styles.bottomContainer}>
              <Icon name="arrow-up" />
              <Text>Slide Up</Text>
            </View>

          </SwipeGesture>
        </ImageBackground>

        <ViewShot
          style={styles.viewStyle}
          ref={(c) => { this._screen = c; }}
          options={{ format: 'jpg', quality: 0.8, result: 'data-uri' }}
        >
          <ImageBackground
            source={{ uri: img }}
            style={{
              height: '100%',
              width: '100%',
              alignItems: 'center',
            }}
            blurRadius={0.5}
          >
            <Text style={[human.calloutWhite, styles.authorText]}>#AkuInspirasi</Text>
            <Text style={
              [human.title2White,
                { textAlign: 'center', marginHorizontal: 16, marginTop: '10%' }]}
            >
              {quote}

            </Text>
            <Text style={[human.calloutWhite, styles.authorText]}>{author}</Text>
          </ImageBackground>
        </ViewShot>

        <Loading isLoading={isLoading} />
      </Container>
    );
  }
}
