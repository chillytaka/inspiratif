import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Icon } from 'native-base';
import PropTypes from 'prop-types';


class BackButton extends Component {
  static propTypes = {
    iconStyle: PropTypes.objectOf(
      PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
    ),
    buttonStyle: PropTypes.objectOf(
      PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
    ),
  }

   static defaultProps ={
     iconStyle: {},
     buttonStyle: {},
   }

   render() {
     const { navigation, iconStyle, buttonStyle } = this.props;
     return (
       <TouchableOpacity
         style={[{ alignSelf: 'flex-start', zIndex: 4 }, buttonStyle]}
         onPress={() => navigation.goBack()}
         {...this.props}
       >
         <Icon name="arrow-back" style={iconStyle} />
       </TouchableOpacity>

     );
   }
}

export default withNavigation(BackButton);
