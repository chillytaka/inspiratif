import React, { Component } from 'react';
import {
  Text, TouchableOpacity, View, StyleSheet,
} from 'react-native';
import { human } from 'react-native-typography';
import PropTypes from 'prop-types';
import colors from '../val/colors';


const styles = StyleSheet.create({
  card: {
    width: '90%',
    backgroundColor: colors.accent,
    alignContent: 'center',
    alignSelf: 'center',
    marginVertical: 10,
    elevation: 4,
  },

  container: {
    padding: 16,
  },
});

export default class ResepItem extends Component {
    static propTypes = {
      title: PropTypes.string,
      idUser: PropTypes.string,
      onPress: PropTypes.func,
    };

    static defaultProps = {
      title: '',
      idUser: '',
      onPress: () => {},
    };

    render() {
      const { title, onPress, idUser } = this.props;
      return (
        <TouchableOpacity style={styles.card} onPress={() => onPress(title, idUser)}>
          <View style={styles.container}>
            <Text style={human.headlineWhite}>{title}</Text>
          </View>
        </TouchableOpacity>
      );
    }
}
