import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Button, Icon, Text } from 'native-base';
import PropTypes from 'prop-types';
import colors from '../val/colors';
import Error from './ErrorToast';

const styles = StyleSheet.create({
  buttonStyle: {
    alignSelf: 'center',
    marginTop: 15,
    backgroundColor: colors.accent,
  },
  textStyle: {
    width: '80%',
    paddingVertical: 20,
  },
});


export default class CustomButton extends Component {
    static propTypes = {
      text: PropTypes.string,
      icon: PropTypes.string,
      onPress: PropTypes.func,
    }

    static defaultProps = {
      text: '',
      icon: undefined,
      onPress: () => { Error('Coming Soon!'); },
    }

    render() {
      const { text, icon, onPress } = this.props;
      return (
        <Button
          rounded
          style={styles.buttonStyle}
          onPress={onPress}
        >
          {icon
            ? <Icon name={icon} /> : null
        }
          <Text style={styles.textStyle}>{text}</Text>
        </Button>
      );
    }
}
