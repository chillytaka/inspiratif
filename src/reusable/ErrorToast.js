import { Toast } from 'native-base';

const Error = (error) => {
  Toast.show({
    text: `${error}`,
    buttonText: 'Okay',
  });
};

export default Error;
