import React from 'react';
import PropTypes from 'prop-types';
import { Spinner } from 'native-base';
import Modal from 'react-native-modal';

const Loading = ({ isLoading }) => (
  <Modal isVisible={isLoading} animationIn="fadeIn" animationOut="fadeOut">
    <Spinner />
  </Modal>
);

Loading.propTypes = {
  isLoading: PropTypes.bool.isRequired,
};

export default Loading;
