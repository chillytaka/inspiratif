import React from 'react';
import { Image, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import logo from '../val/logo';


const styles = StyleSheet.create({
  imageStyle: {
    width: 100,
    height: 100,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
});

const Logo = ({ style }) => (
  <Image source={logo} style={[styles.imageStyle, style]} />
);

Logo.propTypes = {
  style: PropTypes.objectOf(
    PropTypes.string,
  ),
};

Logo.defaultProps = {
  style: {},
};

export default Logo;
