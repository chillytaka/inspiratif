import React, { Component } from 'react';
import { Text, TouchableWithoutFeedback } from 'react-native';
import { material } from 'react-native-typography';
import PropTypes from 'prop-types';

export default class TextButton extends Component {
    static propTypes = {
      text: PropTypes.string.isRequired,
      onPress: PropTypes.func,
      active: PropTypes.bool,
    }

    static defaultProps = {
      onPress: () => {},
      active: false,
    }

    textDisplay = () => {
      const { active, text } = this.props;
      if (!active) {
        return (
          <Text
            style={[material.titleWhite, { textAlign: 'center', color: 'grey' }]}
          >
            {text}
          </Text>
        );
      }
      return (
        <Text
          style={[material.titleWhite, { textAlign: 'center' }]}
        >
          {text}
        </Text>
      );
    }

    render() {
      const { onPress } = this.props;
      return (

        <TouchableWithoutFeedback onPress={onPress}>
          {this.textDisplay()}
        </TouchableWithoutFeedback>

      );
    }
}
