import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { human } from 'react-native-typography';
import PropTypes from 'prop-types';


const styles = StyleSheet.create({
  titleText: {
    marginTop: '5%',
    fontWeight: 'bold',
  },
});

const Title = ({ title }) => (

  <Text style={[human.title2, styles.titleText]}>
    {title}
  </Text>
);

Title.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Title;
