import React, { Component } from 'react';
import { View, Dimensions } from 'react-native';
import { CardItem, Text } from 'native-base';

const { width, height } = Dimensions.get('window');

export class FirstPage extends Component {
  render() {
    return (
      <View style={{ width: width - 33, height }}>
        <CardItem>
          <Text>Nama: Ispandi</Text>
        </CardItem>
        <CardItem>
          <Text>
Alamat :
Taman Sejahtera C 15, Jl. Karangklumprik Utara RT 01/RW 07, Balasklumprik, Wiyung, Surabaya
          </Text>
        </CardItem>
        <CardItem>
          <Text>Telp: 085748735828 </Text>
        </CardItem>
        <CardItem>
          <Text>Lokasi Praktik: Rumah Herbal Toga Citra - Wiyung, Surabaya </Text>
        </CardItem>
        <CardItem>
          <Text>Jam Praktik: 10.00-17.00 WIB </Text>
        </CardItem>
      </View>
    );
  }
}

export default FirstPage;
