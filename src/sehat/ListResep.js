import React, { Component } from 'react';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import firebase from 'react-native-firebase';
import { FlatList } from 'react-native-gesture-handler';
import colors from '../val/colors';
import BackButton from '../reusable/BackButton';
import BoxButton from '../reusable/BoxButton';
import Loading from '../reusable/Loading';


export default class ListResep extends Component {
  constructor(props) {
    super(props);

    this.ref = firebase.firestore().collection('resep');
    this.state = {
      data: [],
      loading: true,
    };
  }

  componentDidMount = () => {
    this.getData();
  }

  // get data and save it to state
  getData = () => {
    let prev = '';
    const tempData = [];

    this.ref.orderBy('name', 'asc').get()
      .then(async (res) => {
        await res.forEach((docs) => {
          const { name } = docs.data();

          const tempItem = {
            key: docs.id, name,
          };

          if (name !== prev) {
            tempData.push(tempItem);
            prev = name;
          }
        });

        this.setState({ data: tempData, loading: false });
      });
  }

  renderer = ({ item }) => (
    <BoxButton title={item.name} onPress={this.onPress} />
  )

  // on user press the button
  onPress = (title) => {
    const { navigation } = this.props;

    navigation.navigate('Resep', { title });
  }

  render() {
    const { data, loading } = this.state;
    return (
      <Container>
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{ height: '100%', padding: 16 }}
        >
          <BackButton />

          <FlatList data={data} renderItem={this.renderer} />
        </LinearGradient>
        <Loading isLoading={loading} />
      </Container>
    );
  }
}
