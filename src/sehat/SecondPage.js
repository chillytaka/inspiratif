import React, { Component } from 'react';
import { View, Dimensions } from 'react-native';
import { CardItem, Text } from 'native-base';

const { width, height } = Dimensions.get('window');

export class FirstPage extends Component {
  render() {
    return (
      <View style={{ width: width - 32, height }}>
        <CardItem>
          <Text>Nama: Elok Subekti</Text>
        </CardItem>
        <CardItem>
          <Text>Pendidikan : Akademi Farmasi Islam</Text>
        </CardItem>
        <CardItem>
          <Text>Lokasi Praktik: Rumah Herbal Toga Citra - Wiyung, Surabaya </Text>
        </CardItem>
        <CardItem>
          <Text>Jam Praktik: 10.00-17.00 WIB </Text>
        </CardItem>
      </View>
    );
  }
}

export default FirstPage;
