/* eslint-disable global-require */
import React, { Component } from 'react';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import Logo from '../reusable/Logo';
import Title from '../reusable/Title';
import colors from '../val/colors';
import CustomButton from '../reusable/CustomButton';
import BackButton from '../reusable/BackButton';

const img = [
  { img: require('../../assets/resep/1.jpg') },
  { img: require('../../assets/resep/2.jpg') },
  { img: require('../../assets/resep/3.jpg') },
  { img: require('../../assets/resep/4.jpg') },
  { img: require('../../assets/resep/5.jpg') },
  { img: require('../../assets/resep/6.jpg') },
];


export default class Sehat extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{ height: '100%', padding: 16 }}
        >
          <BackButton />
          <Logo />
          <Title title="Aku Butuh Sehat !" />

          <CustomButton
            text="Treatment"
            icon="medkit"
            onPress={() => navigation.navigate('Treatment')}
          />
          <CustomButton
            text="Minuman Tradisional"
            icon="flask"
            onPress={() => navigation.navigate('ListResep', { src: img })}
          />
        </LinearGradient>
      </Container>
    );
  }
}
