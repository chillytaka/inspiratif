import React, { Component } from 'react';
import { FlatList, AppState } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Container } from 'native-base';
import Modal from 'react-native-modal';
import firebase from 'react-native-firebase';
import { ScrollView } from 'react-native-gesture-handler';
import SecondPage from './SecondPage';
import Logo from '../reusable/Logo';
import Loading from '../reusable/Loading';
import colors from '../val/colors';
import BackButton from '../reusable/BackButton';
import ListVideo from '../youtube/ListVideo';
import Youtube from '../youtube/Youtube';
import FirstPage from './FirstPage';

export class Treatment extends Component {
  constructor(props) {
    super(props);

    this.treatment = firebase.firestore().collection('treatment');
    this.state = {
      isVisible: false,
      youtubeId: '',
      data: [],
      isLoading: false,
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    const tempData = [];
    this.treatment.get()
      .then(async (res) => {
        await res.forEach((docs) => {
          const { title, videoId } = docs.data();

          const tempItem = {
            key: docs.id, title, youtubeId: videoId,
          };

          tempData.push(tempItem);
        });
        this.setState({ data: tempData, isLoading: false });
      });
  }


  toggleVisible = () => {
    this.setState(prev => ({ isVisible: !prev.isVisible }));
  }

  // set video untuk ditaruh di tampilkan
  setVideo = (youtubeId) => {
    this.setState({ youtubeId }, () => this.toggleVisible());
  }

  renderer = ({ item }) => (
    <ListVideo
      youtubeId={item.youtubeId}
      title={item.title}
      onPress={this.setVideo}
    />
  )

  render() {
    const {
      data, isVisible, youtubeId, isLoading,
    } = this.state;
    return (
      <Container>
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{ height: '100%', padding: 16 }}
        >
          <BackButton />
          <Logo />

          <ScrollView horizontal pagingEnabled>
            <FirstPage />
            <SecondPage />
          </ScrollView>

        </LinearGradient>
        <Loading isLoading={isLoading} />
      </Container>
    );
  }
}

export default Treatment;
