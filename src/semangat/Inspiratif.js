/* eslint-disable global-require */
import React, { Component } from 'react';
import {
  View, StyleSheet, Dimensions, Text,
} from 'react-native';
import {
  Footer, FooterTab, Button,
} from 'native-base';
import Carousel from 'react-native-snap-carousel';
import LinearGradient from 'react-native-linear-gradient';
import firebase from 'react-native-firebase';
import colors from '../val/colors';
import InspiratifItem from './InspiratifItem';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  background: {
    backgroundColor: colors.main2,
  },
  textStyle: {
    color: 'black',
  },
});

export default class Index extends Component {
  constructor(props) {
    super(props);

    this.inspiratif = firebase.firestore().collection('inspirasi');

    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    this.inspiratif.get().then(async (item) => {
      const tempData = [];

      await item.forEach((docs) => {
        const { id } = docs;
        const { text, title } = docs.data();
        const tempItem = { id, text, title };

        tempData.push(tempItem);
      });

      this.setState(prev => ({ data: [...prev.data, ...tempData] }));
    });
  }

// next button Pressed
onNext = () => {
  const { data } = this.state;
  const { currentIndex } = this._carousel;
  if (currentIndex + 1 !== data.length) {
    this._carousel.snapToNext();
  }
}

// prev button Pressed
onPrev = () => {
  this._carousel.snapToPrev();
}


renderer = ({ item }) => (
  <InspiratifItem title={item.title} text={item.text} />
)

render() {
  const { data } = this.state;
  return (
    <LinearGradient
      colors={[colors.main1, colors.main2]}
      style={{ height, width, flex: 1 }}
    >
      <View style={{ flex: 1 }}>
        <Carousel
          data={data}
          renderItem={this.renderer}
          ref={(c) => { this._carousel = c; }}
          sliderWidth={width}
          itemWidth={width}
          slideStyle={{ width }}
          inactiveSlideOpacity={1}
          inactiveSlideScale={1}
        />
      </View>
      <Footer style={styles.background}>
        <FooterTab>
          <Button style={styles.background} onPress={this.onPrev}>
            <Text>Previous Story</Text>
          </Button>
        </FooterTab>


        <FooterTab>
          <Button onPress={this.onNext} style={styles.background}>
            <Text>Other Story</Text>
          </Button>
        </FooterTab>
      </Footer>
    </LinearGradient>
  );
}
}
