import React from 'react';
import { Content } from 'native-base';
import { Dimensions, Text, View } from 'react-native';
import { material } from 'react-native-typography';
import PropTypes from 'prop-types';
import Logo from '../reusable/Logo';
import BackButton from '../reusable/BackButton';


const { height } = Dimensions.get('window');

const InspiratifItem = ({ text, title }) => (
  <View
    style={{ height: height - 55, paddingHorizontal: 16 }}
  >
    <BackButton buttonStyle={{ position: 'absolute', left: 16, top: 16 }} />
    <Logo style={{ width: '20%', height: '10%' }} />
    <Text style={[material.title, { textAlign: 'center' }]}>Kisah Inspiratif</Text>
    <Text style={[material.caption, { textAlign: 'center' }]}>based on true story</Text>
    <Text style={[material.title, { textAlign: 'center' }]}>{title}</Text>
    <Content>
      <Text style={material.body1}>{text.replace(/\\n/g, '\n')}</Text>
    </Content>
  </View>
);

InspiratifItem.propTypes = {
  text: PropTypes.string,
  title: PropTypes.string,
};

InspiratifItem.defaultProps = {
  text: '',
  title: '',
};

export default InspiratifItem;
