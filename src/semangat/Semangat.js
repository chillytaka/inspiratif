/* eslint-disable global-require */
import React, { Component } from 'react';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import Logo from '../reusable/Logo';
import Title from '../reusable/Title';
import colors from '../val/colors';
import CustomButton from '../reusable/CustomButton';
import BackButton from '../reusable/BackButton';

const img = [
  { img: require('../../assets/motivasi/1.jpg') },
  { img: require('../../assets/motivasi/2.jpg') },
  { img: require('../../assets/motivasi/3.jpg') },
  { img: require('../../assets/motivasi/4.jpg') },
  { img: require('../../assets/motivasi/5.jpg') },
  { img: require('../../assets/motivasi/6.jpg') },
];

export default class Semangat extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{ height: '100%', padding: 16 }}
        >
          <BackButton />

          <Logo />
          <Title title="Aku Butuh Semangat !" />

          <CustomButton
            text="Dzikir Pagi dan Sore"
            icon="clock"
            onPress={() => navigation.navigate('PdfView')}
          />
          <CustomButton
            text="Kata Motivasi"
            icon="happy"
            onPress={() => navigation.navigate('Motivasi', { src: img })}
          />
          <CustomButton
            text="Kisah Inspiratif"
            icon="bulb"
            onPress={() => navigation.navigate('Inspiratif')}
          />
        </LinearGradient>
      </Container>
    );
  }
}
