import React, { Component } from 'react';
import {
  StyleSheet, View, Dimensions,
} from 'react-native';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import firebase from 'react-native-firebase';
import colors from '../val/colors';
import BackButton from '../reusable/BackButton';
import SlideItem from './SlideItem';
import getPhoto from '../api/Unsplash';
import SlideResep from './SlideResep';


const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  backStyle: {
    left: 16,
    top: 16,
    elevation: 1,
    zIndex: 1,
    position: 'absolute',
    color: colors.white,
  },
  containerStyle: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: 16,
  },
});


export default class Motivasi extends Component {
  constructor(props) {
    super(props);

    this.ref = firebase.firestore(); // firebase ref
    this.state = {
      active: 0,
      data: [],
    };
  }

  componentDidMount = () => {
    this.getItem();
  }

  // get item from firebase collection
  getItem = () => {
    const { navigation } = this.props;
    const { title } = navigation.state.params;

    if (title !== undefined) {
      // get resep
      this.ref.collection('resep')
        .where('name', '==', title).get()
        .then(res => res.forEach(this.addResep));
    } else {
      // get motivasi
      this.ref.collection('motivasi').get()
        .then((res) => {
          res.forEach(this.addMotivasi);
        });
    }
  }

  // add resep item to state
  addResep = async (docs) => {
    const { data } = this.state;
    const {
      background, bahan, cara, name,
    } = docs.data();

    let bgItem = background;

    // get gambar
    if (background === 'j') {
      await getPhoto().then((img) => {
        bgItem = img;
        firebase.firestore().doc(`resep/${docs.id}`).update({ background: img });
      });
    }

    const tempItem = {
      background: { uri: bgItem }, bahan, cara, name,
    };

    const tempData = [...data, tempItem];

    this.setState({ data: tempData });
  }

  // add motivasi item to state
  addMotivasi = async (docs) => {
    const { data } = this.state;
    const { background, author, quote } = docs.data();
    let bgItem = background;

    if (background === 'j') {
      await getPhoto().then((img) => {
        bgItem = img;
        firebase.firestore().doc(`motivasi/${docs.id}`).update({ background: img });
      });
    }

    const tempItem = {
      background: { uri: bgItem }, author, quote,
    };

    const tempData = [...data, tempItem];
    this.setState({ data: tempData });
  }

  renderItem = ({ item }) => {
    if (item.name === undefined) {
      return (
        <SlideItem
          id={item.id}
          img={item.background}
          quote={item.quote}
          author={item.author}
        />
      );
    }
    return (
      <SlideResep
        img={item.background}
        bahan={item.bahan}
        cara={item.cara}
        name={item.name}
      />
    );
  }

  render() {
    const { active, data } = this.state;
    return (
      <Container>
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{ height: '100%' }}
        >
          <View style={{ flex: 1 }}>
            <BackButton iconStyle={styles.backStyle} />
            <Carousel
              data={data}
              renderItem={this.renderItem}
              sliderWidth={width}
              itemWidth={width}
              slideStyle={{ width }}
              inactiveSlideOpacity={1}
              inactiveSlideScale={1}
              lockScrollWhileSnapping
              loop
              autoplay
              autoplayDelay={1000}
              autoplayInterval={3000}
              onSnapToItem={(index) => { this.setState({ active: index }); }}
            />
            <Pagination
              dotsLength={data.length}
              activeDotIndex={active}
              containerStyle={styles.containerStyle}
            />
          </View>
        </LinearGradient>
      </Container>
    );
  }
}
