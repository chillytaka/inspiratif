import React, { Component } from 'react';
import {
  ImageBackground, View, StyleSheet, Dimensions, Text, Image,
} from 'react-native';
import PropTypes from 'prop-types';
import { material } from 'react-native-typography';
import colors from '../val/colors';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  imgStyles: {
    width,
    height,
    justifyContent: 'center',
  },
  containerStyles: {
    padding: 16,
    backgroundColor: colors.translucent,
  },
  titleStyle: {
    textAlign: 'center',
  },
  textStyle: {
    marginTop: 5,
    textAlign: 'center',
  },
  logoStyle: {
    width: 200,
    height: 150,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
});

const logo = require('../../assets/logo.png');

export default class MotivasiItem extends Component {
    static propTypes = {
      img: PropTypes.shape({
        uri: PropTypes.string.isRequired,
      }),
      author: PropTypes.string,
      quote: PropTypes.string,
    }

    static defaultProps = {
      author: '',
      quote: '',
      img: 'j',
    }

    render() {
      const { author, quote, img } = this.props;
      return (
        <View>
          <ImageBackground source={img} style={styles.imgStyles}>
            <View style={styles.containerStyles}>
              <Text style={[material.subheading, styles.titleStyle]}>{quote}</Text>
              <Text style={[material.body2, styles.textStyle]}>
                -
                {' '}
                {author}
                {' '}
                -
              </Text>
              <Image source={logo} style={styles.logoStyle} />
            </View>
          </ImageBackground>
        </View>
      );
    }
}
