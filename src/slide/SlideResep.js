import React, { Component } from 'react';
import {
  Text,
  Dimensions,
  View,
  StyleSheet,
  ImageBackground,
  Image,
} from 'react-native';
import PropTypes from 'prop-types';
import { material } from 'react-native-typography';
import colors from '../val/colors';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  imgStyles: {
    width,
    height,
    justifyContent: 'center',
  },
  containerStyles: {
    padding: 16,
    backgroundColor: colors.translucent,
  },
  titleStyle: {
    textAlign: 'center',
  },
  textStyle: {
    marginTop: 5,
    textAlign: 'center',
  },
  logoStyle: {
    width: 200,
    height: 150,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
});

const logo = require('../../assets/logo.png');

export default class SlideResep extends Component {
  static propTypes = {
    img: PropTypes.shape({
      uri: PropTypes.string.isRequired,
    }).isRequired,
    bahan: PropTypes.arrayOf(PropTypes.string),
    cara: PropTypes.arrayOf(PropTypes.string),
    name: PropTypes.string,
  }

  static defaultProps = {
    bahan: undefined,
    cara: undefined,
  }

  static defaultProps = {
    name: '',
  }

  // menyebar bahan
  spreadBahan = () => {
    const { bahan } = this.props;
    let itemCount = 1;

    if (bahan !== undefined) {
      return bahan.map((item) => {
        itemCount += 1;

        return (
          <Text key={itemCount}>
            {itemCount - 1}
            .
            {item}
          </Text>
        );
      });
    }
    return null;
  }

  // menyebar cara
  spreadCara = () => {
    const { cara } = this.props;
    let itemCount = 1;

    if (cara !== undefined) {
      return cara.map((item) => {
        itemCount += 1;

        return (
          <Text key={itemCount}>
            {itemCount - 1}
          .
            {item}
          </Text>
        );
      });
    } return null;
  }

  sectionBahan = () => {
    const { bahan } = this.props;

    if (bahan !== undefined) {
      return (
        <View>
          <Text style={[material.body2]}>Bahan</Text>
          {this.spreadBahan()}
        </View>
      );
    } return null;
  }

  sectionCara = () => {
    const { cara } = this.props;
    if (cara !== undefined) {
      return (
        <View>
          <Text style={[material.body2]}>Cara</Text>
          {this.spreadCara()}
        </View>
      );
    } return null;
  }


  render() {
    const { name, img } = this.props;

    return (

      <View>
        <ImageBackground source={img} style={styles.imgStyles}>
          <View style={styles.containerStyles}>
            <Text style={[material.title, styles.titleStyle]}>{name}</Text>

            {this.sectionBahan()}

            {/* separator */}
            <View style={{ height: 10 }} />

            {this.sectionCara()}

            <Image source={logo} style={styles.logoStyle} />
          </View>
        </ImageBackground>
      </View>

    );
  }
}
