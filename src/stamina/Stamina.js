import React, { Component } from 'react';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import Logo from '../reusable/Logo';
import Title from '../reusable/Title';
import colors from '../val/colors';
import CustomButton from '../reusable/CustomButton';
import BackButton from '../reusable/BackButton';

export default class Stamina extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{ height: '100%', padding: 16 }}
        >
          <BackButton />

          <Logo />

          <Title title="Aku Butuh Stamina !" />

          <CustomButton
            text="Video Perawatan"
            icon="trending-up"
            onPress={() => navigation.navigate('Player', { collection: 'perawatan' })}
          />
          <CustomButton
            text="Video Pencegahan"
            icon="warning"
            onPress={() => navigation.navigate('Player', { collection: 'pencegahan' })}
          />
          <CustomButton
            text="Video Penyembuhan"
            icon="pulse"
            onPress={() => navigation.navigate('Player', { collection: 'penyembuhan' })}
          />
        </LinearGradient>
      </Container>
    );
  }
}
