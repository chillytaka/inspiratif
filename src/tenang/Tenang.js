import React, { Component } from 'react';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import Logo from '../reusable/Logo';
import Title from '../reusable/Title';
import colors from '../val/colors';
import CustomButton from '../reusable/CustomButton';
import BackButton from '../reusable/BackButton';

export default class Tenang extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{ height: '100%', padding: 16 }}
        >
          <BackButton />
          <Logo />
          <Title title="Aku Butuh Tenang ! " />
          <CustomButton
            text="Murotal"
            icon="book"
            onPress={() => navigation.navigate('Music', { source: 'murotal' })}
          />
          <CustomButton
            text="Instrumen"
            icon="musical-note"
            onPress={() => navigation.navigate('Music', { source: 'instrumental' })}
          />
          <CustomButton
            text="Natural"
            icon="leaf"
            onPress={() => navigation.navigate('Music', { source: 'natural' })}
          />
        </LinearGradient>
      </Container>
    );
  }
}
