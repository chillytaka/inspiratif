const colors = {
  main: '#2de2f2',
  grey: '#424242',
  accent: '#0086fc',
  accent2: '#09a1f2',
  white: 'white',
  main1: '#49eefc',
  main2: '#0aa0fc',
  inbetween1: '#40c1fc',
  inbetween2: '#34a6fc',
  inbetween3: '#2899fc',
  inbetween4: '#2180fc',
  inbetween5: '#0f7afc',
  inbetween6: '#0058fc',
  red: '#ff4c4f',
  translucent: 'rgba(255, 255, 255, 0.8)',
  transparent: 'rgba(255, 255, 255, 0.0)',
  darkgrey: '#565656',
};

export default colors;
