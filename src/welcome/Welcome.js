import React, { Component } from 'react';
import { material } from 'react-native-typography';
import LinearGradient from 'react-native-linear-gradient';
import {
  Container, Text, Button, Icon,
} from 'native-base';
import {
  StyleSheet, StatusBar, Image, TouchableWithoutFeedback,
} from 'react-native';
import firebase from 'react-native-firebase';
import colors from '../val/colors';
import logo from '../val/logo';

const styles = StyleSheet.create({
  nextButton: {
    backgroundColor: colors.accent,
    position: 'absolute',
    bottom: 80,
    alignSelf: 'center',
    elevation: 5,
  },
});

export default class Welcome extends Component {
  constructor(props) {
    super(props);

    this.unsubscribe = undefined;
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  // checking if user is available
  onNext = () => {
    const { navigation } = this.props;
    this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      navigation.navigate(user ? 'mainNav' : 'authNav');
    });
  }

  render() {
    return (
      <Container>
        <StatusBar hidden />
        <TouchableWithoutFeedback>
          <LinearGradient colors={[colors.main1, colors.main2]} style={{ height: '100%' }}>
            <Text style={[{ margin: 30 }, material.display2]}>Welcome,</Text>
            <Image
              source={logo}
              style={{
                width: '90%',
                height: '50%',
                alignSelf: 'center',
                resizeMode: 'contain',
              }}
            />
            <Button style={styles.nextButton} rounded onPress={this.onNext}>
              <Text>Next</Text>
              <Icon name="arrow-forward" />
            </Button>
          </LinearGradient>
        </TouchableWithoutFeedback>
      </Container>
    );
  }
}
