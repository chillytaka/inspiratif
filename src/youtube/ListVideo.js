import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Thumbnail } from 'react-native-thumbnail-video';
import {
  Left, Body, Card, CardItem, Text,
} from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { material } from 'react-native-typography';

export default class ListVideo extends Component {
    static propTypes = {
      youtubeId: PropTypes.string,
      title: PropTypes.string,
      onPress: PropTypes.func,
    }

     static defaultProps = {
       youtubeId: 'BR_B_cgW_Wc',
       title: '',
       onPress: () => {},
     }

     render() {
       const { youtubeId, title, onPress } = this.props;
       return (
         <Card>
           <TouchableOpacity onPress={() => onPress(youtubeId)}>
             <CardItem>
               <Left>
                 <Thumbnail
                   url={`https://www.youtube.com/watch?v=${youtubeId}`}
                   imageWidth={100}
                   imageHeight={50}
                   showPlayIcon={false}
                   onPress={() => {}}
                 />
               </Left>
               <Body>
                 <Text style={material.body1}>{title}</Text>
               </Body>
             </CardItem>
           </TouchableOpacity>
         </Card>
       );
     }
}
