import React, { Component } from 'react';
import { FlatList, AppState } from 'react-native';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import firebase from 'react-native-firebase';
import Loading from '../reusable/Loading';
import colors from '../val/colors';
import BackButton from '../reusable/BackButton';
import ListVideo from './ListVideo';
import Youtube from './Youtube';

export default class Player extends Component {
  constructor(props) {
    super(props);

    this.listRef = firebase.firestore();
    this.state = {
      isVisible: false,
      youtubeId: '',
      data: [],
      isLoading: true,
    };
  }

  componentDidMount() {
    AppState.addEventListener('change', this._test);
    this.getData();
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._test);
  }

  _test = (nextAppState) => {
    if (nextAppState !== 'active') {
      this.toggleVisible();
    }
  }

  getData =() => {
    const { navigation } = this.props;
    const tempData = [];
    this.listRef.collection(navigation.state.params.collection).get()
      .then(async (res) => {
        await res.forEach((docs) => {
          const { title, videoId } = docs.data();

          const tempItem = {
            key: docs.id, title, youtubeId: videoId,
          };

          tempData.push(tempItem);
        });
        this.setState({ data: tempData, isLoading: false });
      });
  }

  toggleVisible = () => {
    this.setState(prev => ({ isVisible: !prev.isVisible }));
  }

  // set video untuk ditaruh di tampilkan
  setVideo = (youtubeId) => {
    this.setState({ youtubeId }, () => this.toggleVisible());
  }

  renderer = ({ item }) => (
    <ListVideo
      youtubeId={item.youtubeId}
      title={item.title}
      onPress={this.setVideo}
    />
  )

  render() {
    const {
      isVisible, data, youtubeId, isLoading,
    } = this.state;
    return (
      <Container>
        <LinearGradient
          colors={[colors.main1, colors.main2]}
          style={{ height: '100%', padding: 16 }}
        >
          <BackButton />

          <FlatList
            data={data}
            renderItem={this.renderer}
          />

          <Modal
            isVisible={isVisible}
            onBackButtonPress={this.toggleVisible}
            onBackdropPress={this.toggleVisible}
          >
            <Youtube youtubeId={youtubeId} />
          </Modal>
        </LinearGradient>
        <Loading isLoading={isLoading} />
      </Container>
    );
  }
}
