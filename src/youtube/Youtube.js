import React, { Component } from 'react';
import { View } from 'react-native';
import { WebView } from 'react-native-webview';
import PropTypes from 'prop-types';


export default class Youtube extends Component {
    static propTypes = {
      youtubeId: PropTypes.string,
    }

    static defaultProps = {
      youtubeId: '',
    }


    render() {
      const { youtubeId } = this.props;
      return (
        <View style={{
          width: '99%', height: '35%', alignSelf: 'center', justifyContent: 'center',
        }}
        >
          <WebView
            source={{ uri: `https://www.youtube.com/embed/${youtubeId}` }}
            onShouldStartLoadWithRequest={() => false}
          />
        </View>
      );
    }
}
